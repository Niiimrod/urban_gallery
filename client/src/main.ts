import "primeflex/primeflex.css";
import "primeicons/primeicons.css";
// PRIME COMPONENTS
import Button from "primevue/button";
import Column from "primevue/column";
import PrimeVue from "primevue/config";
import ConfirmationService from "primevue/confirmationservice";
import ConfirmDialog from "primevue/confirmdialog";
import DataTable from "primevue/datatable";
import Dialog from "primevue/dialog";
import FileUpload from "primevue/fileupload";
import Galleria from "primevue/galleria";
import InputSwitch from "primevue/inputswitch";
import InputText from "primevue/inputtext";
import Listbox from "primevue/listbox";
import PanelMenu from "primevue/panelmenu";
import Password from "primevue/password";
//THEME
import "primevue/resources/primevue.min.css";
import "primevue/resources/themes/saga-blue/theme.css";
import SelectButton from "primevue/selectbutton";
import TabPanel from "primevue/tabpanel";
import TabView from "primevue/tabview";
import Textarea from "primevue/textarea";
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
//STORE
import store from "./store/index.store";

createApp(App)
  .component("Dialog", Dialog)
  .component("Toast", Toast)
  .component("ConfirmDialog", ConfirmDialog)
  .component("Galleria", Galleria)
  .component("TabView", TabView)
  .component("TabPanel", TabPanel)
  .component("FileUpload", FileUpload)
  .component("Textarea", Textarea)
  .component("Listbox", Listbox)
  .component("InputSwitch", InputSwitch)
  .component("SelectButton", SelectButton)
  .component("Password", Password)
  .component("InputText", InputText)
  .component("Button", Button)
  .component("PanelMenu", PanelMenu)
  .component("DataTable", DataTable)
  .component("Column", Column)
  .use(ConfirmationService)
  .use(ToastService)
  .use(PrimeVue)
  .use(store)
  .use(router)
  .mount("#app");
