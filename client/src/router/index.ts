import Login from "@/components/public/components/auth/components/Login.vue";
import Register from "@/components/public/components/auth/components/Register.vue";
import Home from "@/components/public/Home.vue";
import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/home",
    component: Home,
  },
  {
    path: "/albums",
    name: "albums",
    component: () =>
      import("@/components/public/components/albums/AlbumBoard.vue"),
    redirect: "album-list",
    children: [
      {
        path: "/album-list",
        name: "album-list",
        component: () =>
          import("@/components/public/components/albums/list/AlbumList.vue"),
      },
      {
        path: "/album/:albumId",
        props: true,
        name: "album-gallery",
        component: () =>
          import(
            "@/components/public/components/albums/consult/AlbumConsult.vue"
          ),
      },
      {
        path: "/album/:albumId/gallery/:galleryId",
        props: true,
        name: "album-gallery-consult",
        component: () =>
          import(
            "@/components/public/components/gallery/consult/GalleryConsult.vue"
          ),
      },
    ],
  },

  {
    path: "/login",
    component: Login,
  },
  {
    path: "/register",
    component: Register,
  },
  {
    path: "/profile",
    name: "profile",
    // lazy-loaded
    component: () =>
      import("@/components/public/components/auth/components/Profile.vue"),
  },
  // {
  //   path: "/albums",
  //   name: "albums",
  //   component: () =>
  //     import(
  //       "@/components/dashboard/artists/components/albums/list/AlbumList.vue"
  //     )
  // },
  // {
  //   path: "/admin",
  //   name: "admin",
  //   component: () => import("@/components/dashboard/admin/BoardAdmin.vue"),
  //   redirect: "/admin/users",
  //   children: [
  //     {
  //       path: "/admin/artists",
  //       name: "admin-artists",
  //       component: () =>
  //         import(
  //           "@/components/dashboard/admin/components/artists/list/ArtistList.vue"
  //         )
  //     },
  //     {
  //       path: "/admin/artists/create",
  //       name: "admin-artists-create",
  //       component: () =>
  //         import(
  //           "@/components/dashboard/admin/components/artists/create/ArtistCreate.vue"
  //         )
  //     },
  //     {
  //       path: "/admin/artists/edit/:id",
  //       name: "admin-artists-edit",
  //       props: true,
  //       component: () =>
  //         import(
  //           "@/components/dashboard/admin/components/artists/edit/ArtistEdit.vue"
  //         )
  //     },
  //     {
  //       path: "/admin/artists/edit/:id/albums/create",
  //       name: "admin-artists-edit-albums-create",
  //       props: true,
  //       component: () =>
  //         import(
  //           "@/components/dashboard/shared/components/albums/create/AlbumCreate.vue"
  //         )
  //     },
  //     {
  //       path: "/admin/artists/edit/:id/albums/:albumId/edit",
  //       name: "admin-artists-edit-albums-edit",
  //       props: true,
  //       component: () =>
  //         import(
  //           "@/components/dashboard/shared/components/albums/edit/AlbumEdit.vue"
  //         )
  //     },
  //     {
  //       path: "/admin/users",
  //       name: "admin-users",
  //       component: () =>
  //         import(
  //           "@/components/dashboard/admin/components/users/list/UserList.vue"
  //         )
  //     },
  //     {
  //       path: "/admin/users/create",
  //       name: "admin-user-create",
  //       component: () =>
  //         import(
  //           "@/components/dashboard/admin/components/users/create/UserCreate.vue"
  //         )
  //     },
  //     {
  //       path: "/admin/users/edit/:id",
  //       name: "admin-user-edit",
  //       props: true,
  //       component: () =>
  //         import(
  //           "@/components/dashboard/admin/components/users/edit/UserEdit.vue"
  //         )
  //     }
  // ]
  // }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, _from, next) => {
  const publicPages = [
    "home",
    "login",
    "albums",
    "album-list",
    "album-gallery",
    "album-gallery-consult",
  ];
  const authRequired = !publicPages.includes(to.name as string);
  const loggedIn = localStorage.getItem("user");

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next("/");
  } else {
    next();
  }
});

export default router;
