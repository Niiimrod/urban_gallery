import { CommonService } from "@/components/shared/services/common.service";
import { apiClient } from "@/lib/api-client";
import { GalleryManager } from "@/lib/managers/galleries/gallery.manager";
import { GalleryApiModel } from "@/lib/models/gallery/gallery.apimodel";
import { ID } from "@/lib/models/shared/api.model";
import { GalleryModel } from "../models/gallery.model";
import { ImageService } from "./image.service";

let instance: GalleryService | null = null;
export class GalleryService {
  private galleryManager: GalleryManager;
  private commonService: CommonService;
  private imageService: ImageService;

  public static getInstance(): GalleryService {
    if (!instance) {
      instance = new GalleryService();
    }
    return instance;
  }

  public constructor() {
    "ngInject";
    this.galleryManager = new GalleryManager(apiClient);
    this.commonService = CommonService.getInstance();
    this.imageService = ImageService.getInstance();

    if (!instance) {
      instance = this;
    }
    return instance;
  }

  public getGalleryList(albumId: ID): Promise<GalleryModel[]> {
    return this.galleryManager
      .getAlbumGalleryList(albumId)
      .then(galleryApiModel =>
        this.galleryListApiModeltoGalleryListModel(galleryApiModel)
      );
  }

  public getGallery(albumId: ID, galleryId: ID): Promise<GalleryModel> {
    return this.galleryManager
      .getAlbumGallery(albumId, galleryId)
      .then(galleryApiModel =>
        this.galleryApiModelToGalleryModel(galleryApiModel)
      );
  }

  public updateGallery(albumId: ID, galleryModel: GalleryModel): Promise<void> {
    const result = this.galleryModelToGalleryApiModel(galleryModel);
    return this.galleryManager.updateGallery(albumId, result);
  }

  public postAlbumGallery(
    albumId: ID,
    galleryModel: GalleryModel
  ): Promise<void> {
    const result = this.galleryModelToGalleryApiModel(galleryModel);
    return this.galleryManager.postAlbumGallery(albumId, result);
  }

  //   public deleteAlbum(albumId: ID): Promise<void> {
  //     return this.albumManager.deleteAlbum(albumId);
  //   }

  public galleryListApiModeltoGalleryListModel(
    galleryListApiModel: GalleryApiModel[]
  ): GalleryModel[] {
    return galleryListApiModel.map(item =>
      this.galleryApiModelToGalleryModel(item)
    );
  }

  public galleryListModeltoGalleryListApiModel(
    galleryListModel: GalleryModel[]
  ): GalleryApiModel[] {
    return galleryListModel.map(item =>
      this.galleryModelToGalleryApiModel(item)
    );
  }

  private galleryApiModelToGalleryModel(
    galleryApiModel: GalleryApiModel
  ): GalleryModel {
    return {
      galleryName: galleryApiModel.GalleryName,
      galleryPhoto: this.imageService.imageApiModelToImageModel(
        galleryApiModel.GalleryPhoto
      ),
      galleryImageList: this.imageService.imageListApiModelToImageListModel(
        galleryApiModel.GalleryImageList
      ),
      id: galleryApiModel._id,
      galleryDescription: galleryApiModel.GalleryDescription,
      modificationNotes: this.commonService.modificationNoteListApiModelToModificationNoteListModel(
        galleryApiModel.ModificationNotes
      )
    };
  }

  private galleryModelToGalleryApiModel(
    galleryModel: GalleryModel
  ): GalleryApiModel {
    return {
      GalleryName: galleryModel.galleryName,
      GalleryPhoto: this.imageService.imageModelToImageApiModel(
        galleryModel.galleryPhoto
      ),
      GalleryImageList: this.imageService.imageListModelToImageListApiModel(
        galleryModel.galleryImageList
      ),
      _id: galleryModel.id,
      GalleryDescription: galleryModel.galleryDescription,
      ModificationNotes: this.commonService.modificationNoteListModelToModificationNoteListApiModel(
        galleryModel.modificationNotes
      )
    };
  }
}
