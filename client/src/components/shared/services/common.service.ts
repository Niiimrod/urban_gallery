import { ModificationNotesApiModel } from "@/lib/models/shared/api.model";
import { ModificationNotesModel } from "../models/common.model";

let instance: CommonService | null = null;
export class CommonService {
  public static getInstance(): CommonService {
    if (!instance) {
      instance = new CommonService();
    }
    return instance;
  }

  public constructor() {
    "ngInject";
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  public modificationNoteListApiModelToModificationNoteListModel(
    modificationNoteListApiModel: ModificationNotesApiModel[]
  ): ModificationNotesModel[] {
    return modificationNoteListApiModel.map(modificationNoteApiModel =>
      this.modificationNoteApiModelToModificationNoteModel(
        modificationNoteApiModel
      )
    );
  }

  private modificationNoteApiModelToModificationNoteModel(
    modificationNoteApiModel: ModificationNotesApiModel
  ): ModificationNotesModel {
    return {
      modificationDate: modificationNoteApiModel.ModificationDate,
      modificationNote: modificationNoteApiModel.ModificationNote
    };
  }

  public modificationNoteListModelToModificationNoteListApiModel(
    modificationNoteListApiModel: ModificationNotesModel[]
  ): ModificationNotesApiModel[] {
    return modificationNoteListApiModel.map(modificationNoteApiModel =>
      this.modificationNoteModelToModificationNoteApiModel(
        modificationNoteApiModel
      )
    );
  }

  private modificationNoteModelToModificationNoteApiModel(
    modificationNoteApiModel: ModificationNotesModel
  ): ModificationNotesApiModel {
    return {
      ModificationDate: modificationNoteApiModel.modificationDate,
      ModificationNote: modificationNoteApiModel.modificationNote
    };
  }
}
