import { CommonService } from "@/components/shared/services/common.service";
import { apiClient } from "@/lib/api-client";
import { AlbumManager } from "@/lib/managers/albums/albums.manager";
import { AlbumApiModel } from "@/lib/models/album/album.apiModel";
import { ID } from "@/lib/models/shared/api.model";
import { AlbumModel } from "../models/album.model";
import { GalleryService } from "./gallery.service";
import { ImageService } from "./image.service";

let instance: AlbumService | null = null;
export class AlbumService {
  private albumManager: AlbumManager;
  private commonService: CommonService;
  private imageService: ImageService;
  private galleryService: GalleryService;

  public static getInstance(): AlbumService {
    if (!instance) {
      instance = new AlbumService();
    }
    return instance;
  }

  public constructor() {
    "ngInject";
    this.albumManager = new AlbumManager(apiClient);
    this.commonService = CommonService.getInstance();
    this.imageService = ImageService.getInstance();
    this.galleryService = GalleryService.getInstance();

    if (!instance) {
      instance = this;
    }
    return instance;
  }
  public getAlbumList(): Promise<AlbumModel[]> {
    return this.albumManager
      .getAlbumList()
      .then(albumApiModel =>
        this.albumListApiModeltoAlbumListModel(albumApiModel)
      );
  }

  public getAlbum(albumId: ID): Promise<AlbumModel> {
    return this.albumManager
      .getAlbum(albumId)
      .then(albumApiModel => this.albumApiModelToAlbumModel(albumApiModel));
  }

  public updateAlbum(albumModel: AlbumModel): Promise<void> {
    const albumApiModel = this.albumModelToAlbumApiModel(albumModel);
    return this.albumManager.updateAlbum(albumApiModel);
  }

  public postAlbum(albumModel: AlbumModel): Promise<void> {
    const result = this.albumModelToAlbumApiModel(albumModel);
    return this.albumManager.postAlbum(result);
  }

  public deleteAlbum(albumId: ID): Promise<void> {
    return this.albumManager.deleteAlbum(albumId);
  }

  private albumListApiModeltoAlbumListModel(
    albumListApiModel: AlbumApiModel[]
  ): AlbumModel[] {
    return albumListApiModel.map(item => this.albumApiModelToAlbumModel(item));
  }

  private albumApiModelToAlbumModel(albumApiModel: AlbumApiModel): AlbumModel {
    return {
      albumName: albumApiModel.AlbumName,
      albumPhoto: this.imageService.imageApiModelToImageModel(
        albumApiModel.AlbumPhoto
      ),
      _id: albumApiModel._id,
      albumDescription: albumApiModel.AlbumDescription,
      albumIsPublished: albumApiModel.AlbumIsPublished,
      albumGalleries: albumApiModel.AlbumGalleries
        ? this.galleryService.galleryListApiModeltoGalleryListModel(
            albumApiModel.AlbumGalleries
          )
        : [],
      modificationNotes: this.commonService.modificationNoteListApiModelToModificationNoteListModel(
        albumApiModel.ModificationNotes
      )
    };
  }

  private albumModelToAlbumApiModel(albumModel: AlbumModel): AlbumApiModel {
    console.log("albumModel", albumModel.albumPhoto);
    return {
      AlbumName: albumModel.albumName,
      AlbumPhoto: this.imageService.imageModelToImageApiModel(
        albumModel.albumPhoto
      ),
      _id: albumModel._id ? albumModel._id : undefined,
      AlbumIsPublished: albumModel.albumIsPublished,
      AlbumGalleries: albumModel.albumGalleries
        ? this.galleryService.galleryListModeltoGalleryListApiModel(
            albumModel.albumGalleries
          )
        : [],
      AlbumDescription: albumModel.albumDescription,
      ModificationNotes: this.commonService.modificationNoteListModelToModificationNoteListApiModel(
        albumModel.modificationNotes
      )
    };
  }
}
