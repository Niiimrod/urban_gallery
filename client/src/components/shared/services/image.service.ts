import { apiClient } from "@/lib/api-client";
import { ImageManager } from "@/lib/managers/images/images.manager";
import { ImageApiModel } from "@/lib/models/image/image.apimodel";
import {
  CreateImageModel,
  ImageModel
} from "@/components/shared/models/common.model";
import { CommonService } from "@/components/shared/services/common.service";

let instance: ImageService | null = null;
export class ImageService {
  private commonService: CommonService;
  private imageManager: ImageManager;

  public static getInstance(): ImageService {
    if (!instance) {
      instance = new ImageService();
    }
    return instance;
  }

  public constructor() {
    "ngInject";
    this.commonService = CommonService.getInstance();
    this.imageManager = new ImageManager(apiClient);

    if (!instance) {
      instance = this;
    }
    return instance;
  }

  public postImage(createImageModel: CreateImageModel): Promise<ImageModel> {
    return this.imageManager
      .postUploadImage(createImageModel)
      .then(imageApiModel => this.imageApiModelToImageModel(imageApiModel));
  }

  public postUploadImageList(fileList: any): Promise<ImageModel[]> {
    return this.imageManager
      .postUploadImageList(fileList)
      .then(imageListApiModel =>
        this.imageListApiModelToImageListModel(imageListApiModel)
      );
  }

  public imageListApiModelToImageListModel(
    imageApiModel: ImageApiModel[]
  ): ImageModel[] {
    return imageApiModel.map(image => this.imageApiModelToImageModel(image));
  }

  public imageListModelToImageListApiModel(
    imageModelList: ImageModel[]
  ): ImageApiModel[] {
    return imageModelList.map(image => this.imageModelToImageApiModel(image));
  }

  public imageApiModelToImageModel(imageApiModel: ImageApiModel): ImageModel {
    return {
      _id: imageApiModel._id ? imageApiModel._id : undefined,
      img: imageApiModel.Img,
      name: imageApiModel.Name,
      type: imageApiModel.Type,
      alt: imageApiModel.Alt,
      title: imageApiModel.Title,
      modificationNotes: this.commonService.modificationNoteListApiModelToModificationNoteListModel(
        imageApiModel.ModificationNotes
      )
    };
  }

  public imageModelToImageApiModel(imageModel: ImageModel): ImageApiModel {
    return {
      _id: imageModel._id ? imageModel._id : undefined,
      Img: imageModel.img,
      Name: imageModel.name,
      Type: imageModel.type,
      Alt: imageModel.alt,
      Title: imageModel.title,
      ModificationNotes: imageModel.modificationNotes
        ? this.commonService.modificationNoteListModelToModificationNoteListApiModel(
            imageModel.modificationNotes
          )
        : []
    };
  }
}
