import {
  ImageModel,
  ModificationNotesModel
} from "@/components/shared/models/common.model";

export interface GalleryModel {
  id?: string;
  galleryName: string;
  galleryDescription: string;
  galleryPhoto: ImageModel;
  galleryImageList: ImageModel[];
  modificationNotes: ModificationNotesModel[];
}
