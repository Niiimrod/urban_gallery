import { ID } from "@/lib/models/shared/api.model";
import {
  ImageModel,
  ModificationNotesModel
} from "@/components/shared/models/common.model";
import { GalleryModel } from "./gallery.model";
export interface AlbumModel {
  _id?: ID;
  albumName: string;
  albumDescription: string;
  albumPhoto: ImageModel;
  albumIsPublished: boolean;
  albumGalleries?: GalleryModel[];
  modificationNotes: ModificationNotesModel[];
}
