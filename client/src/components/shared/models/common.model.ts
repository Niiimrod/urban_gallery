import { ID } from "@/lib/models/shared/api.model";

/* STATES */
export interface MainState {
  version: string;
  $localForage: any;
}
export interface ModificationNotesModel {
  modificationDate: Date;
  modificationNote: string;
}
export interface ImageModel {
  _id?: ID;
  img: string;
  name: string;
  type: string;
  alt: string;
  title: string;
  modificationNotes: ModificationNotesModel[];
}

export interface CreateImageModel {
  file: FileList;
  alt: string;
  title: string;
}
