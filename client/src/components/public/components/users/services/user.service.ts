import { apiClient } from "@/lib/api-client";
import { UserManager } from "@/lib/managers/user/user.manager";
import { RoleApiModel } from "@/lib/models/role/role.apimodel";
import { ID } from "@/lib/models/shared/api.model";
import { UserApiModel } from "@/lib/models/user/user.apimodel";
import { CommonService } from "@/components/shared/services/common.service";
import { RoleModel, RoleType, UserModel } from "../models/user.model";

let instance: UserService | null = null;
export class UserService {
  private userManager: UserManager;
  private commonService: CommonService;

  public static getInstance(): UserService {
    if (!instance) {
      instance = new UserService();
    }
    return instance;
  }

  public constructor() {
    "ngInject";
    this.userManager = new UserManager(apiClient);
    this.commonService = CommonService.getInstance();
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  public getUserList(): Promise<UserModel[]> {
    return this.userManager
      .getUserList()
      .then(userApiModel => this.userListApiModelToUserListModel(userApiModel));
  }

  public getUser(userId: ID): Promise<UserModel> {
    return this.userManager
      .getUser(userId)
      .then(userApiModel => this.userApiModelToUserModel(userApiModel));
  }

  public updateUser(userModel: UserModel): Promise<UserModel> {
    const userApiModel = this.userModelToUserApiModel(userModel);
    return this.userManager
      .updateUser(userApiModel)
      .then(userApiModel => this.userApiModelToUserModel(userApiModel));
  }

  private userListApiModelToUserListModel(
    listApiModel: UserApiModel[]
  ): UserModel[] {
    return listApiModel.map(item => this.userApiModelToUserModel(item));
  }

  private userApiModelToUserModel(userApiModel: UserApiModel): UserModel {
    return {
      username: userApiModel.Username,
      email: userApiModel.Email,
      roles: this.roleListApiModeltoRoleListModel(userApiModel.Roles),
      id: userApiModel._id,
      modificationNotes: this.commonService.modificationNoteListApiModelToModificationNoteListModel(
        userApiModel.ModificationNotes
      )
    };
  }

  private roleListApiModeltoRoleListModel(
    roleListApiModel: RoleApiModel[]
  ): RoleModel[] {
    return roleListApiModel.map(roleApiModel =>
      this.roleApiModelToRoleModel(roleApiModel)
    );
  }

  private roleApiModelToRoleModel(roleApiModel: RoleApiModel): RoleModel {
    return {
      name: this.getRoleName(roleApiModel.Name),
      value: roleApiModel.Name.toUpperCase() as RoleType
    };
  }

  private getRoleName(roleApiName: string): string {
    switch (roleApiName) {
      case "user":
        return "Utilisateur";
      case "admin":
        return "Administrateur";
      case "moderator":
        return "Modérateur";
      default:
        return "NONE";
    }
  }

  public postUser(userModel: UserModel): Promise<void> {
    const result = this.userModelToUserApiModel(userModel);
    return this.userManager.postUser(result);
  }

  private userModelToUserApiModel(userModel: UserModel): UserApiModel {
    return {
      Email: userModel.email,
      Roles: this.roleListModelToRoleListApiModel(userModel.roles),
      Username: userModel.username,
      Password: userModel.password,
      _id: userModel.id,
      ModificationNotes: userModel.modificationNotes
        ? this.commonService.modificationNoteListModelToModificationNoteListApiModel(
            userModel.modificationNotes
          )
        : []
    };
  }

  private roleListModelToRoleListApiModel(
    roleListModel: RoleModel[]
  ): RoleApiModel[] {
    return roleListModel.map(roleModel =>
      this.roleModelToRoleApiMode(roleModel)
    );
  }

  private roleModelToRoleApiMode(roleModel: RoleModel): RoleApiModel {
    return {
      Name: roleModel.value.toLocaleLowerCase()
    };
  }

  public deleteUser(userId: ID): Promise<void> {
    return this.userManager.deleteUser(userId);
  }
}
