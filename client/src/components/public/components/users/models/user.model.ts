import { ID } from "@/lib/models/shared/api.model";
import { ModificationNotesModel } from "@/components/shared/models/common.model";

export interface UserModel {
  id?: ID;
  username: string;
  email: string;
  roles: RoleModel[];
  modificationNotes: ModificationNotesModel[];
  password?: string;
  accessToken?: string;
}

export interface RoleModel {
  name: string;
  value: RoleType;
}

export type RoleType = "ADMIN" | "MODERATOR" | "USER" | "ARTIST";

export enum Roles {
  user = "USER",
  admin = "ADMIN",
  moderator = "MODERATOR",
  artist = "ARTIST"
}

export const RoleList: RoleModel[] = [
  { name: "Administrateur", value: Roles.admin },
  { name: "Modérateur", value: Roles.moderator },
  { name: "Utilisateur", value: Roles.user }
];
