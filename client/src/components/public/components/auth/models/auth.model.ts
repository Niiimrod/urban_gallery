import { RoleModel } from "@/components/public/components/users/models/user.model";
import { ID } from "@/lib/models/shared/api.model";

export interface AuthModel {
  username: string;
  email: string;
  roles: RoleModel[];
  accessToken: string;
  id: ID;
  password?: string;
}

export interface SigninModel {
  email: string;
  password: string;
}
