import { CommonService } from "@/components/shared/services/common.service";
import { apiClient } from "@/lib/api-client";
import { AuthManager } from "@/lib/managers/auth/auth.manager";
import { AuthApiModel, SigninApiModel } from "@/lib/models/auth/auth.apimodel";
import { RoleApiModel } from "@/lib/models/role/role.apimodel";
import { UserApiModel } from "@/lib/models/user/user.apimodel";
import { RoleModel, RoleType, UserModel } from "../../users/models/user.model";
import { AuthModel, SigninModel } from "../models/auth.model";

let instance: AuthService | null = null;
export class AuthService {
  private authManager: AuthManager;
  private commonService: CommonService;

  public static getInstance(): AuthService {
    if (!instance) {
      instance = new AuthService();
    }
    return instance;
  }

  public constructor() {
    "ngInject";
    this.authManager = new AuthManager(apiClient);
    this.commonService = CommonService.getInstance();

    if (!instance) {
      instance = this;
    }
    return instance;
  }

  public login(authModel: SigninModel): Promise<AuthModel> {
    const params = this.authModelToAuthApiModel(authModel);
    return this.authManager.login(params).then(response => {
      const result = this.authApiModelToUserModel(response);
      if (result.accessToken) {
        localStorage.setItem("user", JSON.stringify(result));
      }
      return result;
    });
  }

  public logout(): void {
    this.authManager.logout();
  }

  public isTokenValid(): Promise<void> {
    return this.authManager.isTokenValid();
  }

  public register(userModel: UserModel): Promise<UserModel> {
    const params = this.userModelToUserApiModel(userModel);

    return this.authManager
      .register(params)
      .then(userApiModel => this.userApiModelToUserModel(userApiModel));
  }

  private userApiModelToUserModel(userApiModel: UserApiModel): UserModel {
    return {
      username: userApiModel.Username,
      email: userApiModel.Email,
      roles: this.roleListApiModeltoRoleListModel(userApiModel.Roles),
      id: userApiModel._id,
      modificationNotes: this.commonService.modificationNoteListApiModelToModificationNoteListModel(
        userApiModel.ModificationNotes
      )
    };
  }

  private userModelToUserApiModel(userModel: UserModel): UserApiModel {
    return {
      Email: userModel.email,
      Roles: [],
      Username: userModel.username,
      Password: userModel.password,
      ModificationNotes: userModel.modificationNotes
        ? this.commonService.modificationNoteListModelToModificationNoteListApiModel(
            userModel.modificationNotes
          )
        : []
    };
  }

  private authModelToAuthApiModel(authModel: SigninModel): SigninApiModel {
    return {
      Email: authModel.email,
      Password: authModel.password ? authModel.password : ""
    };
  }

  private authApiModelToUserModel(authApiModel: AuthApiModel): AuthModel {
    return {
      roles: this.roleListApiModeltoRoleListModel(authApiModel.Roles),
      id: authApiModel._id,
      accessToken: authApiModel.AccessToken,
      username: authApiModel.Username,
      email: authApiModel.Email
    };
  }

  private roleListApiModeltoRoleListModel(
    roleListApiModel: RoleApiModel[]
  ): RoleModel[] {
    return roleListApiModel.map(roleApiModel =>
      this.roleApiModelToRoleModel(roleApiModel)
    );
  }

  private roleApiModelToRoleModel(roleApiModel: RoleApiModel): RoleModel {
    return {
      name: this.getRoleName(roleApiModel.Name),
      value: roleApiModel.Name.toUpperCase() as RoleType
    };
  }

  private getRoleName(roleApiName: string): string {
    switch (roleApiName) {
      case "user":
        return "Utilisateur";
      case "admin":
        return "Administrateur";
      case "moderator":
        return "Modérateur";
      case "artist":
        return "Artiste";
      default:
        return "NONE";
    }
  }
}
