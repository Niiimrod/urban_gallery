import authHeader from "@/lib/managers/user/auth-header.helper";
import { ID } from "@/lib/models/shared/api.model";
import { UserApiModel } from "@/lib/models/user/user.apimodel";
import { ApiClient } from "../../api-client";

export class UserManager {
  public constructor(private apiClient: ApiClient) {}

  public async getUserList(): Promise<UserApiModel[]> {
    const url = `admin/users`;
    return this.apiClient
      .get(url, { headers: authHeader() })
      .then(response => response.data.DATA);
  }

  public async postUser(userApiModel: UserApiModel): Promise<void> {
    const url = `/admin/users`;
    return this.apiClient.post(url, userApiModel, { headers: authHeader() });
  }

  public async deleteUser(userId: ID): Promise<void> {
    const url = `/admin/users/${userId}`;
    return this.apiClient.delete(url, { headers: authHeader() });
  }

  public async getUser(userId: ID): Promise<UserApiModel> {
    const url = `admin/users/${userId}`;
    return this.apiClient
      .get(url, { headers: authHeader() })
      .then(response => response.data.DATA);
  }

  public async updateUser(userApiModel: UserApiModel): Promise<UserApiModel> {
    const url = `admin/users/${userApiModel._id}`;
    return this.apiClient
      .put(url, userApiModel, { headers: authHeader() })
      .then(response => response.data.DATA);
  }
}
