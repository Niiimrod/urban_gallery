import { CreateImageModel } from "@/components/shared/models/common.model";
import authHeader from "@/lib/managers/user/auth-header.helper";
import { ImageApiModel } from "@/lib/models/image/image.apimodel";
import { ApiClient } from "../../api-client";

export class ImageManager {
  public constructor(private apiClient: ApiClient) {}

  public async postUploadImage(
    createImageModel: CreateImageModel
  ): Promise<ImageApiModel> {
    const url = `/admin/upload/single`;
    const formData = new FormData();
    formData.append("file", createImageModel.file[0]);
    formData.append("alt", createImageModel.alt);
    formData.append("title", createImageModel.title);
    return this.apiClient
      .post(url, formData, { headers: authHeader() })
      .then(response => response.data.DATA);
  }

  public async postUploadImageList(fileList: any): Promise<ImageApiModel[]> {
    const url = `/admin/upload/multiple`;
    const formData = new FormData();
    fileList.forEach((element: FileList) => {
      formData.append("files", element[0]);
    });

    return this.apiClient
      .post(url, formData, { headers: authHeader() })
      .then(response => response.data.DATA);
  }
}
