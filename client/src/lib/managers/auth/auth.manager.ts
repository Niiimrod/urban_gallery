import { AuthApiModel, SigninApiModel } from "@/lib/models/auth/auth.apimodel";
import { UserApiModel } from "@/lib/models/user/user.apimodel";
import { ApiClient } from "../../api-client";
import authHeader from "../user/auth-header.helper";

export class AuthManager {
  public constructor(private apiClient: ApiClient) {}

  public async login(signApiModel: SigninApiModel): Promise<AuthApiModel> {
    const url = `auth/signin`;
    const params = {
      Email: signApiModel.Email,
      Password: signApiModel.Password,
    };
    return this.apiClient
      .post(url, params)
      .then((response) => response.data.DATA);
  }

  public logout() {
    localStorage.removeItem("user");
  }

  public async isTokenValid(): Promise<void> {
    const url = `/admin/istokenvalid`;

    return this.apiClient
      .get(url, { headers: authHeader() })
      .then((response) => response.data)
      .catch(() => {
        console.log("logout");
        this.logout();
      });
  }

  public async register(userModel: UserApiModel): Promise<UserApiModel> {
    const url = `auth/signup`;
    return this.apiClient
      .post(url, userModel)
      .then((response) => response.data.DATA);
  }
}
