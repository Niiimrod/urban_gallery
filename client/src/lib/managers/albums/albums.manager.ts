import authHeader from "@/lib/managers/user/auth-header.helper";
import { AlbumApiModel } from "@/lib/models/album/album.apiModel";
import { ID } from "@/lib/models/shared/api.model";
import { ApiClient } from "../../api-client";

export class AlbumManager {
  public constructor(private apiClient: ApiClient) {}

  public async postAlbum(albumApiModel: AlbumApiModel): Promise<void> {
    const url = `/admin/albums`;
    return this.apiClient.post(url, albumApiModel, { headers: authHeader() });
  }

  public async deleteAlbum(albumId: ID): Promise<void> {
    const url = `/admin/album/${albumId}`;
    return this.apiClient.delete(url, { headers: authHeader() });
  }

  public async updateAlbum(albumApiModel: AlbumApiModel): Promise<void> {
    const url = `admin/albums/${albumApiModel._id}`;
    return this.apiClient.put(url, albumApiModel, { headers: authHeader() });
  }

  public async getAlbum(albumId: ID): Promise<AlbumApiModel> {
    const url = `/albums/${albumId}`;
    return this.apiClient.get(url).then(response => response.data.DATA);
  }

  public async getAlbumList(): Promise<AlbumApiModel[]> {
    const url = `/albums`;
    return this.apiClient.get(url).then(response => response.data.DATA);
  }
}
