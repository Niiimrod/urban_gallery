import authHeader from "@/lib/managers/user/auth-header.helper";
import { GalleryApiModel } from "@/lib/models/gallery/gallery.apimodel";
import { ID } from "@/lib/models/shared/api.model";
import { ApiClient } from "../../api-client";

export class GalleryManager {
  public constructor(private apiClient: ApiClient) {}

  public async postAlbumGallery(
    albumId: ID,
    galleryApiModel: GalleryApiModel
  ): Promise<void> {
    const url = `admin/albums/${albumId}/gallery`;
    return this.apiClient.post(url, galleryApiModel, { headers: authHeader() });
  }

  public async getAlbumGalleryList(albumId: ID): Promise<GalleryApiModel[]> {
    const url = `/album/${albumId}/galleries`;
    return this.apiClient.get(url).then(response => response.data.DATA);
  }

  public async getAlbumGallery(
    albumId: ID,
    galleryId: ID
  ): Promise<GalleryApiModel> {
    const url = `/album/${albumId}/gallery/${galleryId}`;
    return this.apiClient.get(url).then(response => response.data.DATA);
  }

  public async updateGallery(
    albumId: ID,
    galleryApiModel: GalleryApiModel
  ): Promise<void> {
    const url = `admin/albums/${albumId}/gallery/${galleryApiModel._id}`;
    return this.apiClient.put(url, galleryApiModel, { headers: authHeader() });
  }
}
