import { RoleApiModel } from "../role/role.apimodel";
import { ID, ModificationNotesApiModel } from "../shared/api.model";

export interface UserApiModel {
  _id?: ID;
  Username: string;
  Email: string;
  Roles: RoleApiModel[];
  ModificationNotes: ModificationNotesApiModel[];
  Password?: string;
  AccessToken?: string;
}
