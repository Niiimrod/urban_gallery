import { RoleApiModel } from "../role/role.apimodel";
import { ID } from "../shared/api.model";

export interface AuthApiModel {
  Email: string;
  Password: string;
  Roles: RoleApiModel[];
  Username: string;
  AccessToken: string;
  _id: ID;
}

export interface SigninApiModel {
  Email: string;
  Password: string;
}
