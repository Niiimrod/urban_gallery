import { ID } from "../shared/api.model";

export interface RoleApiModel {
  ID?: ID;
  Name: string;
}
