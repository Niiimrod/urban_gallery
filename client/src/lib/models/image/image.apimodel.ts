import { ModificationNotesApiModel } from "../shared/api.model";

export interface ImageApiModel {
  _id?: string;
  Img: string;
  Name: string;
  Type: string;
  Alt: string;
  Title: string;
  ModificationNotes: ModificationNotesApiModel[];
}
