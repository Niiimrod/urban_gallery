import { GalleryApiModel } from "../gallery/gallery.apimodel";
import { ImageApiModel } from "../image/image.apimodel";
import { ID, ModificationNotesApiModel } from "../shared/api.model";

export interface AlbumApiModel {
  _id?: ID;
  AlbumName: string;
  AlbumDescription: string;
  AlbumPhoto: ImageApiModel;
  AlbumIsPublished: boolean;
  AlbumGalleries?: GalleryApiModel[];
  ModificationNotes: ModificationNotesApiModel[];
}
