import { ImageApiModel } from "../image/image.apimodel";
import { ModificationNotesApiModel } from "../shared/api.model";

export interface GalleryApiModel {
  _id?: string;
  GalleryName: string;
  GalleryDescription: string;
  GalleryPhoto: ImageApiModel;
  GalleryImageList: ImageApiModel[];
  ModificationNotes: ModificationNotesApiModel[];
}
