import { AlbumApiModel } from "../album/album.apiModel";
import { ImageApiModel } from "../image/image.apimodel";
import { RoleApiModel } from "../role/role.apimodel";
import { ModificationNotesApiModel } from "../shared/api.model";

export interface ArtistApiModel {
  _id?: string;
  ArtistFirstname?: string;
  ArtistLastname?: string;
  ArtistName: string;
  ArtistWebsite: string;
  ArtistDescription: string;
  ArtistPhoto: ImageApiModel;
  ArtistEmail: string;
  ArtistAlbums: AlbumApiModel[];
  ArtistPassword?: string;
  ArtistRoles?: RoleApiModel[];
  ModificationNotes: ModificationNotesApiModel[];
}
