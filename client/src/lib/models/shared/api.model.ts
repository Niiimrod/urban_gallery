export type ID = string;

export interface ModificationNotesApiModel {
  ModificationDate: Date;
  ModificationNote: string;
}
