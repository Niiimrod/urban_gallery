import axios, { AxiosInstance } from "axios";
export type ApiClient = AxiosInstance;
export const apiClient: ApiClient = axios.create({
  baseURL: "http://localhost:3000/api"
});
