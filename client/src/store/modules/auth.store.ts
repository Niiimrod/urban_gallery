import {
  AuthModel,
  SigninModel
} from "@/components/public/components/auth/models/auth.model";
import { AuthService } from "@/components/public/components/auth/services/auth.service.";
import {
  RoleModel,
  Roles,
  UserModel
} from "@/components/public/components/users/models/user.model";
import { MainState } from "@/components/shared/models/common.model";
import { ActionTree, GetterTree, Module, MutationTree } from "vuex";
const service = new AuthService();
const storedUser = localStorage.getItem("user");

interface LogginStatusModel {
  loggedIn: boolean;
}

export interface AuthStateModel {
  status: LogginStatusModel;
  user: UserModel | null;
}

export const state: AuthStateModel = {
  status: storedUser ? { loggedIn: true } : { loggedIn: false },
  user: storedUser ? JSON.parse(storedUser) : null
};

const mutations: MutationTree<AuthStateModel> = {
  loginSuccess(state, user): void {
    state.status.loggedIn = true;
    state.user = user;
  },
  loginFailure(state): void {
    state.status.loggedIn = false;
    state.user = null;
  },
  logout(state): void {
    state.status.loggedIn = false;
    state.user = null;
  },
  registerSuccess(state): void {
    state.status.loggedIn = false;
  },
  registerFailure(state): void {
    state.status.loggedIn = false;
  },
  updateUser(state, user): void {
    state.user = user;
  }
};

const actions: ActionTree<AuthStateModel, MainState> = {
  login({ commit }, params: SigninModel): Promise<AuthModel> {
    return service.login(params).then(
      user => {
        commit("loginSuccess", user);
        return Promise.resolve(user);
      },
      error => {
        commit("loginFailure");
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        return Promise.reject(message);
      }
    );
  },
  signOut({ commit }): void {
    service.logout();
    commit("logout");
  },
  isTokenValid(): void {
    service.isTokenValid();
  },
  register({ commit }, params: UserModel): Promise<UserModel> {
    return service.register(params).then(
      user => {
        commit("registerSuccess");
        return Promise.resolve(user);
      },
      error => {
        commit("registerFailure");
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        return Promise.reject(message);
      }
    );
  },
  updateUser({ commit }, user: UserModel) {
    commit("updateUser", user);
  }
};

const getters: GetterTree<AuthStateModel, MainState> = {
  isLoggedIn(state): boolean {
    return state.status.loggedIn;
  },
  isAdminOrModerator(state): boolean {
    if (!state.user) {
      return false;
    }
    return (
      state.user &&
      (state.user.roles.some((role: RoleModel) => role.value === Roles.admin) ||
        state.user.roles.some(
          (role: RoleModel) => role.value === Roles.moderator
        ))
    );
  },
  isAdmin(state): boolean {
    if (!state.user) {
      return false;
    }
    return (
      state.user &&
      state.user.roles.some((role: RoleModel) => role.value === Roles.admin)
    );
  }
};

const namespaced = true;

export const authStore: Module<AuthStateModel, MainState> = {
  namespaced,
  state,
  mutations,
  actions,
  getters
};
