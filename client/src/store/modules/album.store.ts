import { AlbumService } from "@/components/shared/services/album.service";
import { ID } from "@/lib/models/shared/api.model";
import { MainState } from "@/components/shared/models/common.model";
import { ActionTree, GetterTree, Module, MutationTree } from "vuex";
import { AlbumModel } from "@/components/shared/models/album.model";

const service = AlbumService.getInstance();

export interface AlbumState {
  albumList: AlbumModel[];
  currentAlbum: AlbumModel | null;
}

export const state: AlbumState = {
  albumList: [],
  currentAlbum: {} as AlbumModel
};

const mutations: MutationTree<AlbumState> = {
  setAlbumList(state, payload: AlbumModel[]) {
    state.albumList = payload;
  },
  setCurrentAlbum(state, payload: AlbumModel) {
    state.currentAlbum = payload;
  },
  clearCurrentAlbum(state) {
    state.currentAlbum = null;
  }
};

const actions: ActionTree<AlbumState, MainState> = {
  getAlbumList({ commit }): void {
    service.getAlbumList().then(albumModelList => {
      commit("setAlbumList", albumModelList);
    });
  },

  async getAlbum({ commit }, albumId: ID): Promise<void> {
    return service
      .getAlbum(albumId)
      .then(currentAlbum => commit("setCurrentAlbum", currentAlbum));
  },

  clearCurrentAlbum({ commit }): void {
    commit("clearCurrentAlbum");
  },

  async postAlbum(_, albumModel): Promise<void> {
    return service
      .postAlbum(albumModel)
      .then(() => this.dispatch("albumStore/getAlbumList"));
  },

  async deleteAlbum(_, albumId: ID): Promise<void> {
    return service
      .deleteAlbum(albumId)
      .then(() => this.dispatch("albumStore/getAlbumList"));
  },

  async putAlbum(_, albumModel): Promise<void> {
    return service
      .updateAlbum(albumModel)
      .then(() => this.dispatch("albumStore/getAlbumList"));
  }
};

const getters: GetterTree<AlbumState, MainState> = {
  getAlbum(state): AlbumModel | null {
    return state.currentAlbum;
  },
  getAlbumList(state): AlbumModel[] | null {
    return state.albumList;
  }
};

const namespaced = true;

export const albumStore: Module<AlbumState, MainState> = {
  namespaced,
  state,
  mutations,
  actions,
  getters
};
