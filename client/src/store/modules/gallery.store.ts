import { MainState } from "@/components/shared/models/common.model";
import { GalleryModel } from "@/components/shared/models/gallery.model";
import { GalleryService } from "@/components/shared/services/gallery.service";
import { ID } from "@/lib/models/shared/api.model";
import { ActionTree, GetterTree, Module, MutationTree } from "vuex";

const service = GalleryService.getInstance();

export interface GalleryState {
  currentGallery: GalleryModel | null;
}

export const state: GalleryState = {
  currentGallery: {} as GalleryModel
};

const mutations: MutationTree<GalleryState> = {
  setCurrentGallery(state, payload: GalleryModel) {
    state.currentGallery = payload;
  },
  clearCurrentGallery(state) {
    state.currentGallery = null;
  }
};

const actions: ActionTree<GalleryState, MainState> = {
  // getGalleryList({ commit }, albumId: ID): void {
  //   service.getGalleryList(albumId).then((galleryListModel) => {
  //     commit("setGalleryList", galleryListModel);
  //   });
  // },

  async getGallery(
    { commit },
    params: { albumId: ID; galleryId: ID }
  ): Promise<void> {
    return service
      .getGallery(params.albumId, params.galleryId)
      .then(currentGallery => commit("setCurrentGallery", currentGallery));
  },

  clearCurrentGallery({ commit }): void {
    commit("clearCurrentGallery");
  },

  async postGallery(
    _,
    params: { albumId: ID; galleryModel: GalleryModel }
  ): Promise<void> {
    return service
      .postAlbumGallery(params.albumId, params.galleryModel)
      .then(() => this.dispatch("albumStore/getAlbum", params.albumId));
  },

  //   async deleteAlbum(_, albumId: ID): Promise<void> {
  //     return service
  //       .deleteAlbum(albumId)
  //       .then(() => this.dispatch("albumStore/getAlbumList"));
  //   },

  async putGallery(
    { commit },
    params: { albumId: ID; galleryModel: GalleryModel }
  ): Promise<void> {
    console.log("galleryModel", params.galleryModel);
    return service
      .updateGallery(params.albumId, params.galleryModel)
      .then(() => {
        this.dispatch("galleryStore/getGallery", {
          albumId: params.albumId,
          galleryId: params.galleryModel.id
        });
      });
  }
};

const getters: GetterTree<GalleryState, MainState> = {
  getCurrentGallery(state): GalleryModel | null {
    return state.currentGallery;
  }
};

const namespaced = true;

export const galleryStore: Module<GalleryState, MainState> = {
  namespaced,
  state,
  mutations,
  actions,
  getters
};
