import { createLogger, createStore } from "vuex";
import { albumStore } from "./modules/album.store";
import { authStore } from "./modules/auth.store";
import { galleryStore } from "./modules/gallery.store";

const debug = process.env.NODE_ENV !== "production";

export default createStore({
  modules: {
    authStore,
    albumStore,
    galleryStore
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
});
