"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoleRoutes = void 0;
const authController_1 = require("../controllers/authController");
const tokenController_1 = require("../controllers/tokenController");
const userRoleController_1 = require("../controllers/userRoleController");
class UserRoleRoutes {
    constructor() {
        this.tokenController = new tokenController_1.TokenController();
        this.authController = new authController_1.AuthController();
        this.userRoleController = new userRoleController_1.UserRoleController();
    }
    route(app) {
        app.use((_, res, next) => {
            res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
            next();
        });
        app.get("/api/admin/users", [this.tokenController.verifyToken, this.authController.isAdmin], this.userRoleController.getUserRoleList);
        app.post("/api/admin/users", [
            this.tokenController.verifyToken,
            this.authController.isAdmin,
            this.authController.checkDuplicateUsernameOrEmail,
            this.authController.checkRolesExisted,
        ], this.userRoleController.postUserRole);
        app.put("/api/admin/users/:id", [
            this.tokenController.verifyToken,
            this.authController.isAdmin,
        ], this.userRoleController.updateUserRole);
        app.delete("/api/admin/users/:id", [this.tokenController.verifyToken, this.authController.isAdmin], this.userRoleController.deleteUserRoleList);
        app.get("/api/admin/users/:id", [this.tokenController.verifyToken, this.authController.isAdmin], this.userRoleController.getOneUserRole);
    }
}
exports.UserRoleRoutes = UserRoleRoutes;
