"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArticleRoutes = void 0;
const auth_controller_1 = require("../controllers/auth.controller");
const token_controller_1 = require("../controllers/token.controller");
const article_controller_1 = require("../controllers/article.controller");
class ArticleRoutes {
    constructor() {
        this.articleController = new article_controller_1.ArticleController();
        this.tokenController = new token_controller_1.TokenController();
        this.authController = new auth_controller_1.AuthController();
    }
    route(app) {
        app.post("/api/admin/articles", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
            this.authController.checkDuplicateUsernameOrEmail,
            this.authController.checkRolesExisted,
        ], this.articleController.postArticle);
        app.put("/api/admin/articles/:id", (req, res) => {
            this.articleController.updateArticle(req, res);
        });
        app.delete("/api/admin/articles/:id", (req, res) => {
            this.articleController.deleteArticle(req, res);
        });
        app.get("/api/articles", (req, res) => {
            this.articleController.getArticleList(req, res);
        });
        app.get("/api/articles/:id", (req, res) => {
            this.articleController.getArticle(req, res);
        });
    }
}
exports.ArticleRoutes = ArticleRoutes;
