"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GalleryRoutes = void 0;
const auth_controller_1 = require("../controllers/auth.controller");
const gallery_controller_1 = require("../controllers/gallery.controller");
const token_controller_1 = require("../controllers/token.controller");
class GalleryRoutes {
    constructor() {
        this.tokenController = new token_controller_1.TokenController();
        this.authController = new auth_controller_1.AuthController();
        this.galleryController = new gallery_controller_1.GalleryController();
    }
    route(app) {
        app.use((_, res, next) => {
            res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
            next();
        });
        app.post("/api/admin/albums/:albumId/gallery", [this.tokenController.verifyToken, this.authController.isAdmin], this.galleryController.postOneAlbumGallery);
        app.put("/api/admin/albums/:albumId/gallery/:galleryId", [this.tokenController.verifyToken, this.authController.isAdmin], this.galleryController.putGallery);
        app.get("/api/album/:albumId/galleries", this.galleryController.getAlbumGalleryList);
        app.get("/api/album/:albumId/gallery/:galleryId", this.galleryController.getOneGallery);
    }
}
exports.GalleryRoutes = GalleryRoutes;
