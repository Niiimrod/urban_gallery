"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthRoutes = void 0;
const auth_controller_1 = require("../controllers/auth.controller");
const token_controller_1 = require("../controllers/token.controller");
class AuthRoutes {
    constructor() {
        this.authController = new auth_controller_1.AuthController();
        this.tokenController = new token_controller_1.TokenController();
    }
    route(app) {
        app.use((_req, res, next) => {
            res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
            next();
        });
        app.get("/api/admin/istokenvalid", [this.tokenController.verifyToken, this.authController.isAdmin], this.authController.isTokenValid);
        app.post("/api/auth/signup", [
            this.authController.checkDuplicateUsernameOrEmail,
            this.authController.checkRolesExisted,
        ], this.authController.signUp);
        app.post("/api/auth/signin", this.authController.signIn);
    }
}
exports.AuthRoutes = AuthRoutes;
