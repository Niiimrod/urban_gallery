"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadRoutes = void 0;
const upload_controller_1 = require("../controllers/upload.controller");
const service_1 = require("../modules/upload/service");
class UploadRoutes {
    constructor() {
        this.uploadService = new service_1.default();
        this.uploadController = new upload_controller_1.UploadController();
    }
    route(app) {
        app.use((_, res, next) => {
            res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
            next();
        });
        app.post("/api/admin/upload/single", this.uploadService.upload.single("file"), this.uploadController.postImage);
        app.post("/api/admin/upload/multiple", this.uploadService.upload.array("files", 10), this.uploadController.postImageList);
    }
}
exports.UploadRoutes = UploadRoutes;
