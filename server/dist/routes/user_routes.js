"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoutes = void 0;
const auth_controller_1 = require("../controllers/auth.controller");
const token_controller_1 = require("../controllers/token.controller");
const user_controller_1 = require("../controllers/user.controller");
class UserRoutes {
    constructor() {
        this.tokenController = new token_controller_1.TokenController();
        this.authController = new auth_controller_1.AuthController();
        this.userController = new user_controller_1.UserController();
    }
    route(app) {
        app.use((_, res, next) => {
            res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
            next();
        });
        app.get("/api/admin/users", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
        ], this.userController.getUserList);
        app.post("/api/admin/users", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
            this.authController.checkDuplicateUsernameOrEmail,
            this.authController.checkRolesExisted,
        ], this.userController.postUser);
        app.put("/api/admin/users/:id", [this.tokenController.verifyToken], this.userController.putUser);
        app.delete("/api/admin/users/:id", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
        ], this.userController.deleteUser);
        app.get("/api/admin/users/:id", [this.tokenController.verifyToken], this.userController.getOneUser);
    }
}
exports.UserRoutes = UserRoutes;
