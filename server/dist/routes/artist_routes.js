"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArtistRoutes = void 0;
const album_controller_1 = require("../controllers/album.controller");
const artist_controller_1 = require("../controllers/artist.controller");
const auth_controller_1 = require("../controllers/auth.controller");
const gallery_controller_1 = require("../controllers/gallery.controller");
const token_controller_1 = require("../controllers/token.controller");
const upload_controller_1 = require("../controllers/upload.controller");
// import * as multer from "multer";
const service_1 = require("../modules/upload/service");
class ArtistRoutes {
    constructor() {
        this.tokenController = new token_controller_1.TokenController();
        this.authController = new auth_controller_1.AuthController();
        this.artistController = new artist_controller_1.ArtistController();
        this.uploadService = new service_1.default();
        this.albumController = new album_controller_1.AlbumController();
        this.galleryController = new gallery_controller_1.GalleryController();
        this.uploadController = new upload_controller_1.UploadController();
    }
    route(app) {
        app.use((_, res, next) => {
            res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
            next();
        });
        app.get("/api/admin/artists", this.artistController.getArtistList);
        app.post("/api/admin/artists", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
            this.artistController.checkDuplicateArtistNameOrEmailOnCreation,
        ], this.artistController.postArtist);
        app.get("/api/admin/artists/:id", 
        // [
        //   this.tokenController.verifyToken,
        //   this.authController.isAdminOrModeratorOrArtist,
        // ],
        this.artistController.getOneArtist);
        app.delete("/api/admin/artists/:id", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
        ], this.artistController.deleteArtist);
        app.post("/api/admin/artists/:id/albums", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModeratorOrArtist,
        ], this.albumController.postOneArtistAlbum);
        app.post("/api/admin/artists/:id/albums/:albumId/gallery", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModeratorOrArtist,
        ], this.galleryController.postOneAlbumGallery);
        app.get("/api/admin/artists/:id/albums/:albumId/gallery", 
        // [
        //   this.tokenController.verifyToken,
        //   this.authController.isAdminOrModeratorOrArtist,
        // ],
        this.galleryController.getAlbumGalleries);
        app.get("/api/admin/artists/:id/albums/:albumId", 
        // [
        //   this.tokenController.verifyToken,
        //   this.authController.isAdminOrModerator,
        // ],
        this.albumController.getOneArtistAlbum);
        app.put("/api/admin/artists/:id", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModeratorOrArtist,
            this.artistController.checkDuplicateArtistNameOrEmailOnEdition,
        ], this.artistController.putArtist);
        app.post("/api/admin/artists", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
            this.artistController.checkDuplicateArtistNameOrEmailOnCreation,
            this.uploadService.upload.array("files", 10),
        ], this.artistController.postArtist);
    }
}
exports.ArtistRoutes = ArtistRoutes;
