"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlbumRoutes = void 0;
const gallery_controller_1 = require("../controllers/gallery.controller");
const album_controller_1 = require("../controllers/album.controller");
const auth_controller_1 = require("../controllers/auth.controller");
const token_controller_1 = require("../controllers/token.controller");
class AlbumRoutes {
    constructor() {
        this.albumController = new album_controller_1.AlbumController();
        this.tokenController = new token_controller_1.TokenController();
        this.authController = new auth_controller_1.AuthController();
        this.galleryController = new gallery_controller_1.GalleryController();
    }
    route(app) {
        app.use((_, res, next) => {
            res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
            next();
        });
        app.delete("/api/admin/album/:albumId", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
        ], this.albumController.deleteAlbum);
        app.get("/api/albums", this.albumController.getAlbumList);
        app.get("/api/albums/:albumId", this.albumController.getOneAlbum);
        app.post("/api/admin/albums", [
            this.tokenController.verifyToken,
            this.authController.isAdminOrModerator,
        ], this.albumController.postOneAlbum);
    }
}
exports.AlbumRoutes = AlbumRoutes;
