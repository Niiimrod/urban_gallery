"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Config = require("../config/auth.config.json");
const model_1 = require("../modules/common/model");
const service_1 = require("../modules/common/service");
const service_2 = require("../modules/roles/service");
const schema_1 = require("../modules/users/schema");
const service_3 = require("../modules/users/service");
class AuthController {
    constructor() {
        this.userService = new service_3.default();
        this.roleService = new service_2.default();
        this.checkDuplicateUsernameOrEmail = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            const userNameParams = { Username: req.body.Username };
            const userEmailParams = { Email: req.body.Email };
            try {
                const userNameExist = yield this.userService.findOneUser(userNameParams);
                const userEmailExist = yield this.userService.findOneUser(userEmailParams);
                if (userNameExist || userEmailExist) {
                    return service_1.userAlreadyExist(res);
                }
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
            next();
        });
        this.checkRolesExisted = (req, res, next) => {
            if (req.body.Roles) {
                for (let i = 0; i < req.body.Roles.length; i++) {
                    if (!model_1.RoleList.includes(req.body.Roles[i].Name)) {
                        service_1.roleDoesNotExist(`Failed! Role ${req.body.Roles[i].Name} does not exist!`, res);
                        return;
                    }
                }
            }
            next();
        };
        this.signUp = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const userParams = new schema_1.default({
                Username: req.body.Username,
                Email: req.body.Email,
                Password: bcrypt.hashSync(req.body.Password, 8),
                ModificationNotes: service_1.getModificationNote("User Created"),
            });
            try {
                const role = yield this.roleService.findOneRole({ Name: "user" });
                if (!role) {
                    return service_1.failureResponse("role does not exist", null, res);
                }
                userParams.Roles = [role];
                const userCreate = yield this.userService.createUser(userParams);
                if (!userCreate) {
                    return service_1.failureResponse("can't create user", null, res);
                }
                return service_1.successResponse("user added successfully", userCreate, res);
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
        });
        this.isTokenValid = (_, res) => __awaiter(this, void 0, void 0, function* () {
            return service_1.successResponse("user added successfully", null, res);
        });
        this.signIn = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let userFind;
            try {
                const signinParams = { Email: req.body.Email };
                userFind = yield this.userService.findOneUserAndPopulate(signinParams);
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
            if (userFind) {
                return this.signinUser(req, res, userFind);
            }
        });
        this.isAdmin = (req, res, next) => {
            this.userHasRole(req, res, next, model_1.Roles.admin);
        };
        this.isModerator = (req, res, next) => {
            this.userHasRole(req, res, next, model_1.Roles.moderator);
        };
        this.isAdminOrModerator = (req, res, next) => {
            this.userHasRoles(req, res, next, [model_1.Roles.moderator, model_1.Roles.admin]);
        };
    }
    signinUser(req, res, params) {
        const passwordIsValid = bcrypt.compareSync(req.body.Password, params.Password);
        if (!passwordIsValid) {
            return service_1.unauthorizedTokenResponse(res);
        }
        const token = jwt.sign({ id: params.id }, Config.secret, {
            expiresIn: 86400,
        });
        return service_1.successResponse("signin succesfully", {
            _id: params.id,
            Username: params.Username,
            Roles: params.Roles,
            AccessToken: token,
        }, res);
    }
    userHasRole(req, res, next, roleName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.userService.findOneUser({
                    _id: req.params.UserId,
                });
                if (!user) {
                    return service_1.notFoundResponse("User not found", res);
                }
                const roleList = yield this.roleService.findRoleList({
                    _id: { $in: user.Roles },
                });
                if (!roleList) {
                    return service_1.notFoundResponse("Can't load roleList", res);
                }
                for (let i = 0; i < roleList.length; i++) {
                    if (roleList[i].Name === roleName) {
                        next();
                    }
                    else {
                        return service_1.roleRequiredResponse(`Require ${roleName} Role!`, res);
                    }
                }
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
        });
    }
    userHasRoles(req, res, next, roleNameList) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.userService.findOneUser({
                    _id: req.params.UserId,
                });
                if (!user) {
                    return service_1.notFoundResponse("User not found", res);
                }
                const roleList = yield this.roleService.findRoleList({
                    _id: { $in: user.Roles },
                });
                for (let i = 0; i < roleList.length; i++) {
                    if (roleNameList.some((roleName) => roleName === roleList[i].Name)) {
                        next();
                        return;
                    }
                }
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
            return service_1.roleRequiredResponse(`Require Admin or Moderator Roles!`, res);
        });
    }
}
exports.AuthController = AuthController;
