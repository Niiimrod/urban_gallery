"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArticleController = void 0;
const schema_1 = require("../modules/articles/schema");
const service_1 = require("../modules/articles/service");
const service_2 = require("../modules/common/service");
class ArticleController {
    constructor() {
        this.articleService = new service_1.default();
    }
    createArticle(req, res) {
        if (req.body.Name &&
            req.body.User &&
            req.body.Title &&
            req.body.Description &&
            req.body.ImgSrc) {
            const articleParams = new schema_1.default({
                Name: req.body.Name,
                User: req.body.User,
                Title: req.body.Title,
                Description: req.body.Description,
                IsDeleted: req.body.IsDeleted,
                ImgSrc: req.body.ImgSrc,
                ModificationNotes: [
                    {
                        modified_on: new Date(Date.now()),
                        modified_by: null,
                        modification_note: "New article created",
                    },
                ],
            });
            this.articleService.createArticle(articleParams, (err, articleData) => {
                if (err) {
                    return service_2.mongoError(err, res);
                }
                else {
                    service_2.successResponse("create article successfull", articleData, res);
                }
            });
        }
        else {
            // error response if some fields are missing in request body
            return service_2.insufficientParameters(res);
        }
    }
    getArticle(req, res) {
        if (req.params.id) {
            const params = { _id: req.params.id };
            this.articleService.filterArticle(params, (err, articleData) => {
                if (err) {
                    return service_2.mongoError(err, res);
                }
                else {
                    service_2.successResponse("get article successfull", articleData, res);
                }
            });
        }
        else {
            return service_2.insufficientParameters(res);
        }
    }
    getArticleList(req, res) {
        this.articleService.getArticleList({}, (err, articleListData) => {
            if (err) {
                return service_2.mongoError(err, res);
            }
            else {
                service_2.successResponse("update article successfull", articleListData, res);
            }
        });
    }
    updateArticle(req, res) {
        if (req.body.Name ||
            req.body.User ||
            req.body.Title ||
            req.body.Description ||
            req.body.IsDeleted ||
            req.body.ImgSrc ||
            req.body.ModificationNotes) {
            const params = { _id: req.params.id };
            this.articleService.filterArticle(params, (err, articleData) => {
                if (err) {
                    return service_2.mongoError(err, res);
                }
                else if (articleData) {
                    articleData.ModificationNotes.push({
                        modified_on: new Date(Date.now()),
                        modified_by: null,
                        modification_note: "Article data updated",
                    });
                    const articleParams = new schema_1.default({
                        _id: req.params.id,
                        Name: req.body.Name ? req.body.Name : articleData.Name,
                        User: req.body.User ? req.body.User : articleData.User,
                        Title: req.body.Title ? req.body.Title : articleData.Title,
                        Description: req.body.Description
                            ? req.body.Description
                            : articleData.Description,
                        IsDeleted: req.body.IsDeleted
                            ? req.body.IsDeleted
                            : articleData.IsDeleted,
                        ImgSrc: req.body.ImgSrc ? req.body.ImgSrc : articleData.ImgSrc,
                        ModificationNotes: articleData.ModificationNotes,
                    });
                    this.articleService.updateArticle(articleParams, (err) => {
                        if (err) {
                            return service_2.mongoError(err, res);
                        }
                        else {
                            service_2.successResponse("update article successfull", null, res);
                        }
                    });
                }
                else {
                    return service_2.failureResponse("invalid article", null, res);
                }
            });
        }
        else {
            return service_2.insufficientParameters(res);
        }
    }
    deleteArticle(req, res) {
        if (req.params.id) {
            this.articleService.deleteArticle(req.params.id, (err, deleteDetails) => {
                if (err) {
                    return service_2.mongoError(err, res);
                }
                else if (deleteDetails.deletedCount !== 0) {
                    service_2.successResponse("delete article successfull", null, res);
                }
                else {
                    return service_2.failureResponse("invalid article", null, res);
                }
            });
        }
        else {
            return service_2.insufficientParameters(res);
        }
    }
}
exports.ArticleController = ArticleController;
