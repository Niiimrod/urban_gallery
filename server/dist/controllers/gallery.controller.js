"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GalleryController = void 0;
const service_1 = require("../modules/albums/service");
const service_2 = require("../modules/common/service");
const schema_1 = require("../modules/galleries/schema");
const service_3 = require("../modules/galleries/service");
const service_4 = require("../modules/images/service");
class GalleryController {
    constructor() {
        this.albumService = new service_1.default();
        this.imageService = new service_4.default();
        this.galleryService = new service_3.default();
        this.postOneAlbumGallery = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.albumId) {
                const galleryParams = new schema_1.default({
                    GalleryName: req.body.GalleryName,
                    GalleryDescription: req.body.GalleryDescription,
                    GalleryPhoto: req.body.GalleryPhoto,
                    GalleryImageList: req.body.GalleryImageList,
                    ModificationNotes: service_2.getModificationNote("Gallery Created"),
                });
                try {
                    const galleryPhoto = yield this.imageService.findOneImage({
                        _id: req.body.GalleryPhoto._id,
                    });
                    if (!galleryPhoto) {
                        return service_2.failureResponse("image does not exist", null, res);
                    }
                    // galleryParams.GalleryPhoto = galleryPhoto;
                    // const galleryImageList = await this.imageService.findImageList({
                    //   $in: req.body.GalleryImageList.map(
                    //     (galleryImage: IImage) => galleryImage._id
                    //   ),
                    // });
                    // if (!galleryImageList) {
                    //   return failureResponse("galleryImageList does not exist", null, res);
                    // }
                    // galleryParams.GalleryImageList = galleryImageList;
                    const galleryCreate = yield this.galleryService.createGallery(galleryParams);
                    if (!galleryCreate) {
                        return service_2.failureResponse("invalid album response", null, res);
                    }
                    const albumUpdate = yield this.albumService.addGalleryToAlbum(req.params.albumId, galleryCreate);
                    if (!albumUpdate) {
                        return service_2.failureResponse("invalid albumUpdate response", null, res);
                    }
                    return service_2.successResponse("album added successfully", albumUpdate, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
        this.putGallery = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.albumId) {
                let currentGallery;
                try {
                    const params = { _id: req.params.galleryId };
                    currentGallery = yield this.galleryService.filterGallery(params);
                    const galleryParams = new schema_1.default({
                        GalleryName: req.body.GalleryName
                            ? req.body.GalleryName
                            : currentGallery.GalleryName,
                        GalleryDescription: req.body.GalleryDescription
                            ? req.body.GalleryDescription
                            : currentGallery.GalleryDescription,
                        GalleryPhoto: req.body.GalleryPhoto
                            ? req.body.GalleryPhoto
                            : currentGallery.GalleryPhoto,
                        GalleryImageList: req.body.GalleryImageList
                            ? req.body.GalleryImageList
                            : currentGallery.GalleryImageList,
                        _id: req.params.galleryId,
                        ModificationNotes: service_2.getModificationNote("Gallery Updated"),
                    });
                    const updateGallery = yield this.galleryService.updateGallery(galleryParams);
                    if (!updateGallery) {
                        return service_2.failureResponse("gallery update failled", null, res);
                    }
                    return service_2.successResponse("update gallery successfull", null, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
        this.getAlbumGalleryList = (_req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const galleryList = yield this.galleryService.getGalleryList({});
                return service_2.successResponse("get galleryList successfull", galleryList, res);
            }
            catch (error) {
                return service_2.mongoError(error, res);
            }
        });
        this.getOneGallery = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.galleryId) {
                try {
                    const gallery = yield this.galleryService.filterGallery({
                        _id: req.params.galleryId,
                    });
                    if (!gallery) {
                        return service_2.notFoundResponse("Gallery Not Found", res);
                    }
                    return service_2.successResponse("get gallery successfull", gallery, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
    }
}
exports.GalleryController = GalleryController;
