"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadController = void 0;
const service_1 = require("../modules/common/service");
const schema_1 = require("../modules/images/schema");
const service_2 = require("../modules/images/service");
const service_3 = require("../modules/upload/service");
class UploadController {
    constructor() {
        this.uploadService = new service_3.default();
        this.imageService = new service_2.default();
        this.postImage = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const file = this.uploadService.uploadFile(req);
            const nameAndType = this.uploadService.getFileNameAndFileType(req);
            const imageParams = new schema_1.default({
                Img: file,
                Name: nameAndType.Name,
                Type: nameAndType.Type,
                Alt: String(req.body.alt),
                Title: String(req.body.title),
                ModificationNotes: service_1.getModificationNote("Image Created"),
            });
            try {
                const image = yield this.imageService.createImage(imageParams);
                if (!image) {
                    return service_1.failureResponse("image fail", null, res);
                }
                return service_1.successResponse("image added successfully", image, res);
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
        });
        this.postImageList = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const files = this.uploadService.uploadFile(req);
            let promiseArray = [];
            for (var i = 0; i < req.files.length; i++) {
                const artistParams = new schema_1.default({
                    Img: files[i],
                    Name: req.files[i].filename,
                    Type: req.files[i].mimetype,
                    ModificationNotes: service_1.getModificationNote("Image Created"),
                });
                promiseArray.push(this.imageService.createImage(artistParams));
            }
            Promise.all(promiseArray)
                .then((imageList) => {
                return service_1.successResponse("image added successfully", imageList, res);
            })
                .catch((error) => {
                return service_1.mongoError(error, res);
            });
        });
    }
}
exports.UploadController = UploadController;
