"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Config = require("../config/auth.config.json");
const model_1 = require("../modules/common/model");
const service_1 = require("../modules/common/service");
const service_2 = require("../modules/roles/service");
const schema_1 = require("../modules/user-role/schema");
const service_3 = require("../modules/user-role/service");
class AuthController {
    constructor() {
        this.userRoleService = new service_3.default();
        this.roleService = new service_2.default();
        this.checkDuplicateUsernameOrEmail = (req, res, next) => {
            const usernameParams = { Username: req.body.Username };
            this.userRoleService.findOneUserRole(usernameParams, (err, userRoleData) => {
                if (err) {
                    return service_1.mongoError(err, res);
                }
                else if (userRoleData) {
                    service_1.userAlreadyExist(res);
                }
                else {
                    const emailParams = { Email: req.body.Email };
                    this.userRoleService.findOneUserRole(emailParams, (err, userRoleData) => {
                        if (err) {
                            return service_1.mongoError(err, res);
                        }
                        else if (userRoleData) {
                            service_1.userAlreadyExist(res);
                        }
                        else {
                            next();
                        }
                    });
                }
            });
        };
        this.checkRolesExisted = (req, res, next) => {
            if (req.body.Roles) {
                for (let i = 0; i < req.body.Roles.length; i++) {
                    if (!model_1.RoleList.includes(req.body.Roles[i].Name)) {
                        service_1.roleDoesNotExist(`Failed! Role ${req.body.Roles[i].Name} does not exist!`, res);
                        return;
                    }
                }
            }
            next();
        };
        this.signUp = (req, res) => {
            const userParams = new schema_1.default({
                Username: req.body.Username,
                Email: req.body.Email,
                Password: bcrypt.hashSync(req.body.Password, 8),
            });
            this.userRoleService.createUserRole(userParams, (err, userRole) => {
                if (err) {
                    return service_1.mongoError(err, res);
                }
                else {
                    this.roleService.findOneRole({ Name: "user" }, (err, role) => {
                        if (err) {
                            return service_1.failureResponse("role does not exist", null, res);
                        }
                        userRole.Roles = [role._id];
                        this.userRoleService.createUserRole(userRole, (err, userRole) => {
                            if (err) {
                                return service_1.failureResponse("invalid user role", null, res);
                            }
                            service_1.successResponse("user role added successfully", userRole, res);
                        });
                    });
                }
            });
        };
        this.signIn = (req, res) => {
            const usernameParams = { Username: req.body.Username };
            this.userRoleService.findOneUserRoleAndPopulate(usernameParams, (err, userRoleData) => {
                if (err) {
                    return service_1.mongoError(err, res);
                }
                if (!userRoleData) {
                    return service_1.notFoundResponse("User Not Found", res);
                }
                const passwordIsValid = bcrypt.compareSync(req.body.Password, userRoleData.Password);
                if (!passwordIsValid) {
                    return service_1.unauthorizedTokenResponse(res);
                }
                const token = jwt.sign({ id: userRoleData.id }, Config.secret, {
                    expiresIn: 86400,
                });
                service_1.successResponse("user signin succesfully", {
                    _id: userRoleData.id,
                    Username: userRoleData.Username,
                    Email: userRoleData.Email,
                    Roles: userRoleData.Roles,
                    AccessToken: token,
                }, res);
            });
        };
        this.isAdmin = (req, res, next) => {
            this.userHasRole(req, res, next, model_1.Roles.admin);
        };
        this.isModerator = (req, res, next) => {
            this.userHasRole(req, res, next, model_1.Roles.moderator);
        };
    }
    userHasRole(req, res, next, roleName) {
        const usernameParams = { _id: req.params.UserId };
        this.userRoleService.findOneUserRole(usernameParams, (err, userRoleData) => {
            if (err) {
                return service_1.mongoError(err, res);
            }
            else {
                const roleParams = { _id: { $in: userRoleData.Roles } };
                this.roleService.findRoleList(roleParams, (err, roles, _) => {
                    if (err) {
                        return service_1.mongoError(err, res);
                    }
                    for (let i = 0; i < roles.length; i++) {
                        if (roles[i].Name === roleName) {
                            next();
                        }
                        else {
                            return service_1.roleRequiredResponse(`Require ${roleName} Role!`, res);
                        }
                    }
                });
            }
        });
    }
}
exports.AuthController = AuthController;
