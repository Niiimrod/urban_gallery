"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlbumController = void 0;
const schema_1 = require("../modules/albums/schema");
const service_1 = require("../modules/albums/service");
const service_2 = require("../modules/common/service");
const service_3 = require("../modules/images/service");
class AlbumController {
    constructor() {
        this.albumService = new service_1.default();
        this.imageService = new service_3.default();
        this.postOneAlbum = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const albumParams = new schema_1.default({
                AlbumName: req.body.AlbumName,
                AlbumDescription: req.body.AlbumDescription,
                AlbumPhoto: req.body.AlbumPhoto,
                AlbumGalleries: req.params.AlbumGalleries,
                AlbumIsPublished: req.body.AlbumIsPublished,
                ModificationNotes: service_2.getModificationNote("Album Created"),
            });
            try {
                const albumImage = yield this.imageService.findOneImage({
                    _id: req.body.AlbumPhoto._id,
                });
                if (!albumImage) {
                    return service_2.failureResponse("image does not exist", null, res);
                }
                albumParams.AlbumPhoto = albumImage;
                const albumCreate = yield this.albumService.createAlbum(albumParams);
                if (!albumCreate) {
                    return service_2.failureResponse("invalid album response", null, res);
                }
                return service_2.successResponse("album added successfully", albumCreate, res);
            }
            catch (error) {
                return service_2.mongoError(error, res);
            }
        });
        this.getOneAlbum = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.albumId) {
                try {
                    const album = yield this.albumService.filterAlbum({
                        _id: req.params.albumId,
                    });
                    if (!album) {
                        return service_2.notFoundResponse("Album Not Found", res);
                    }
                    return service_2.successResponse("get album successfull", album, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
        this.getAlbumList = (_req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const albumList = yield this.albumService.getAlbumList({});
                return service_2.successResponse("get albumList successfull", albumList, res);
            }
            catch (error) {
                return service_2.mongoError(error, res);
            }
        });
        this.deleteAlbum = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.albumId) {
                try {
                    const albumDeleted = yield this.albumService.deleteAlbum(req.params.albumId);
                    if (albumDeleted === null) {
                        return service_2.failureResponse("invalid album", null, res);
                    }
                    return service_2.successResponse("delete album successfull", null, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
    }
}
exports.AlbumController = AlbumController;
