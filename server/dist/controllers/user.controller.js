"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const bcrypt = require("bcryptjs");
const service_1 = require("../modules/common/service");
const service_2 = require("../modules/roles/service");
const schema_1 = require("../modules/users/schema");
const service_3 = require("../modules/users/service");
class UserController {
    constructor() {
        this.userService = new service_3.default();
        this.roleService = new service_2.default();
        this.getUserList = (_req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const userList = yield this.userService.findUserList({});
                if (!userList) {
                    return service_1.failureResponse("can't load roleList", null, res);
                }
                return service_1.successResponse("get userListResponse successfull", userList, res);
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
        });
        this.getOneUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.id) {
                try {
                    const userFind = yield this.userService.findOneUserAndPopulate({
                        _id: req.params.id,
                    });
                    if (!userFind) {
                        return service_1.notFoundResponse("User Not Found", res);
                    }
                    return service_1.successResponse("get user successfull", userFind, res);
                }
                catch (error) {
                    return service_1.mongoError(error, res);
                }
            }
            else {
                return service_1.insufficientParameters(res);
            }
        });
        this.deleteUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.id) {
                try {
                    const userDeleted = yield this.userService.deleteUser(req.params.id);
                    if (userDeleted === null) {
                        return service_1.failureResponse("invalid user", null, res);
                    }
                    return service_1.successResponse("delete user successfull", null, res);
                }
                catch (error) {
                    return service_1.mongoError(error, res);
                }
            }
            else {
                return service_1.insufficientParameters(res);
            }
        });
        this.postUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const userParams = new schema_1.default({
                Username: req.body.Username,
                Email: req.body.Email,
                Password: bcrypt.hashSync(req.body.Password, 8),
                ModificationNotes: service_1.getModificationNote("User Created"),
            });
            if (req.body.Roles.length) {
                const roleNames = req.body.Roles.map((role) => role.Name);
                try {
                    const roleList = yield this.roleService.findRoleList({
                        Name: { $in: roleNames },
                    });
                    if (!roleList) {
                        return service_1.failureResponse("can't find any roles", null, res);
                    }
                    userParams.Roles = roleList.map((role) => role);
                    this.createUser(userParams, res);
                }
                catch (error) {
                    return service_1.mongoError(error, res);
                }
            }
            else {
                try {
                    const role = yield this.roleService.findOneRole({ Name: "user" });
                    if (!role) {
                        return service_1.failureResponse("role does not exist", null, res);
                    }
                    userParams.Roles = [role];
                    this.createUser(userParams, res);
                }
                catch (error) {
                    return service_1.mongoError(error, res);
                }
            }
        });
        this.putUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.body.Username ||
                req.body.Email ||
                req.body.Password ||
                req.body.Roles) {
                let user;
                try {
                    user = yield this.userService.findOneUserAndPopulate({
                        _id: req.params.id,
                    });
                    if (!user) {
                        return service_1.failureResponse("user does not exist", null, res);
                    }
                    user.ModificationNotes.push(service_1.getModificationNote("User Updated"));
                    const newUser = new schema_1.default({
                        Username: req.body.Username ? req.body.Username : user.Username,
                        Email: req.body.Email ? req.body.Email : user.Email,
                        Password: req.body.Password
                            ? bcrypt.hashSync(req.body.Password, 8)
                            : user.Password,
                        _id: req.params.id,
                        ModificationNotes: user.ModificationNotes,
                    });
                    if (req.body.Roles.length) {
                        const roleNames = req.body.Roles.map((role) => role.Name);
                        const roleList = yield this.roleService.findRoleList({
                            Name: { $in: roleNames },
                        });
                        if (!roleList) {
                            return service_1.failureResponse("can't load roleList", null, res);
                        }
                        newUser.Roles = roleList.map((role) => role);
                        this.updateUser(newUser, res);
                    }
                    else {
                        const role = yield this.roleService.findOneRole({ Name: "user" });
                        if (!role) {
                            return service_1.failureResponse("role does not exist", null, res);
                        }
                        newUser.Roles = [role];
                        this.updateUser(newUser, res);
                    }
                }
                catch (error) {
                    return service_1.mongoError(error, res);
                }
            }
            else {
                return service_1.insufficientParameters(res);
            }
        });
    }
    createUser(user, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let createUser;
            try {
                createUser = yield this.userService.createUser(user);
                if (!createUser) {
                    return service_1.failureResponse("invalid user role", null, res);
                }
                return service_1.successResponse("user role added successfully", createUser, res);
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
        });
    }
    updateUser(user, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userUpdated = yield this.userService.updateUser(user);
                if (!userUpdated) {
                    return service_1.failureResponse("can't update user", null, res);
                }
                const populatedUser = yield this.userService.findOneUserAndPopulate({
                    _id: userUpdated._id,
                });
                if (!populatedUser) {
                    return service_1.failureResponse("can't find user", null, res);
                }
                return service_1.successResponse("user updated successfully", populatedUser, res);
            }
            catch (error) {
                return service_1.mongoError(error, res);
            }
        });
    }
}
exports.UserController = UserController;
