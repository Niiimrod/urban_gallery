"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArtistController = void 0;
const bcrypt = require("bcryptjs");
const schema_1 = require("../modules/artists/schema");
const service_1 = require("../modules/artists/service");
const service_2 = require("../modules/common/service");
const service_3 = require("../modules/images/service");
const service_4 = require("../modules/roles/service");
class ArtistController {
    constructor() {
        this.artistService = new service_1.default();
        this.roleService = new service_4.default();
        this.imageService = new service_3.default();
        this.getArtistList = (_req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const artistList = yield this.artistService.getArtistList({});
                return service_2.successResponse("get artistList successfull", artistList, res);
            }
            catch (error) {
                return service_2.mongoError(error, res);
            }
        });
        this.postArtist = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const artistParams = new schema_1.default({
                ArtistFirstname: req.body.ArtistFirstname,
                ArtistLastname: req.body.ArtistLastname,
                ArtistName: req.body.ArtistName,
                ArtistWebsite: req.body.ArtistWebsite,
                ArtistEmail: req.body.ArtistEmail,
                ArtistDescription: req.body.ArtistDescription,
                ArtistAlbums: req.body.ArtistAlbums,
                ArtistPassword: bcrypt.hashSync(req.body.ArtistPassword, 8),
                ModificationNotes: service_2.getModificationNote("Artist Created"),
            });
            try {
                const artistRole = yield this.roleService.findOneRole({ Name: "artist" });
                if (!artistRole) {
                    return service_2.failureResponse("role does not exist", null, res);
                }
                artistParams.ArtistRoles = [artistRole];
                const artistImage = yield this.imageService.findOneImage({
                    _id: req.body.ArtistPhoto._id,
                });
                if (!artistImage) {
                    return service_2.failureResponse("image does not exist", null, res);
                }
                artistParams.ArtistPhoto = artistImage;
                const artistCreate = yield this.artistService.createArtist(artistParams);
                if (!artistCreate) {
                    return service_2.failureResponse("invalid artist params", null, res);
                }
                return service_2.successResponse("artist added successfully", artistCreate, res);
            }
            catch (error) {
                return service_2.mongoError(error, res);
            }
        });
        this.checkDuplicateArtistNameOrEmailOnEdition = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            const artistParams = { _id: { $ne: req.params.id } };
            let artistList;
            try {
                artistList = yield this.artistService.getArtistList(artistParams);
                if (artistList.some((artist) => artist.ArtistName === req.body.ArtistName) ||
                    artistList.some((artist) => artist.ArtistEmail === req.body.ArtistEmail)) {
                    return service_2.userAlreadyExist(res);
                }
                else {
                    next();
                }
            }
            catch (error) {
                return service_2.mongoError(error, res);
            }
        });
        this.checkDuplicateArtistNameOrEmailOnCreation = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let artistNameParams = { ArtistName: req.body.ArtistName };
            let artistEmailParams = { ArtistEmail: req.body.ArtistEmail };
            try {
                const artistNameExist = yield this.artistService.filterArtist(artistNameParams);
                const artistEmailExist = yield this.artistService.filterArtist(artistEmailParams);
                if (artistNameExist || artistEmailExist) {
                    return service_2.userAlreadyExist(res);
                }
                next();
            }
            catch (err) {
                return service_2.mongoError(err, res);
            }
        });
        this.getOneArtist = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.id) {
                try {
                    const artist = yield this.artistService.findOneArtistAndPopulate({
                        _id: req.params.id,
                    });
                    if (!artist) {
                        return service_2.notFoundResponse("User Not Found", res);
                    }
                    return service_2.successResponse("get user successfull", artist, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
        this.deleteArtist = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.id) {
                try {
                    const artistDeleted = yield this.artistService.deleteArtist(req.params.id);
                    if (artistDeleted === null) {
                        return service_2.failureResponse("invalid artist", null, res);
                    }
                    return service_2.successResponse("delete artist successfull", null, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
        this.putArtist = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.body.ArtistFirstname ||
                req.body.ArtistLastname ||
                req.body.ArtistName ||
                req.body.ArtistWebsite ||
                req.body.ArtistDescription ||
                req.body.ArtistPhoto ||
                req.body.ArtistEmail ||
                req.body.ArtistAlbums ||
                req.body.ArtistPassword) {
                let currentArtist;
                try {
                    const params = { _id: req.params.id };
                    currentArtist = yield this.artistService.findOneArtistAndPopulate(params);
                    currentArtist.ModificationNotes.push(service_2.getModificationNote("Artist Updated"));
                    const artistParams = new schema_1.default({
                        ArtistFirstname: req.body.ArtistFirstname
                            ? req.body.ArtistFirstname
                            : currentArtist.ArtistFirstname,
                        ArtistLastname: req.body.ArtistLastname
                            ? req.body.ArtistLastname
                            : currentArtist.ArtistLastname,
                        ArtistName: req.body.ArtistName
                            ? req.body.ArtistName
                            : currentArtist.ArtistName,
                        ArtistWebsite: req.body.ArtistWebsite
                            ? req.body.ArtistWebsite
                            : currentArtist.ArtistWebsite,
                        ArtistDescription: req.body.ArtistDescription
                            ? req.body.ArtistDescription
                            : currentArtist.ArtistDescription,
                        ArtistPhoto: req.body.ArtistPhoto
                            ? req.body.ArtistPhoto
                            : currentArtist.ArtistPhoto,
                        ArtistEmail: req.body.ArtistEmail
                            ? req.body.ArtistEmail
                            : currentArtist.ArtistEmail,
                        ArtistAlbums: req.body.ArtistAlbums
                            ? req.body.ArtistAlbums
                            : currentArtist.ArtistAlbums,
                        Password: req.body.ArtistPassword
                            ? bcrypt.hashSync(req.body.ArtistPassword, 8)
                            : currentArtist.ArtistPassword,
                        _id: req.params.id,
                        ModificationNotes: currentArtist.ModificationNotes,
                    });
                    const updatedArtist = yield this.artistService.updateArtist(artistParams);
                    if (!updatedArtist) {
                        return service_2.failureResponse("artist update failled", null, res);
                    }
                    return service_2.successResponse("update artist successfull", null, res);
                }
                catch (error) {
                    return service_2.mongoError(error, res);
                }
            }
            else {
                return service_2.insufficientParameters(res);
            }
        });
    }
}
exports.ArtistController = ArtistController;
