"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenController = void 0;
const auth_service_1 = require("../modules/auth/auth.service");
const service_1 = require("../modules/common/service");
class TokenController {
    constructor() {
        this.authService = new auth_service_1.default();
        this.verifyToken = (req, res, next) => {
            let token = req.headers["x-access-token"];
            if (!token) {
                return service_1.tokenFailureResponse(res);
            }
            this.authService.verifyToken(token, (err, decoded) => {
                if (err) {
                    return service_1.mongoError(err, res);
                }
                else {
                    req.params.UserId = decoded.id;
                    next();
                }
            });
        };
    }
}
exports.TokenController = TokenController;
