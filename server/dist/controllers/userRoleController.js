"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoleController = void 0;
const bcrypt = require("bcryptjs");
const service_1 = require("../modules/common/service");
const service_2 = require("../modules/roles/service");
const schema_1 = require("../modules/user-role/schema");
const service_3 = require("../modules/user-role/service");
class UserRoleController {
    constructor() {
        this.userRoleService = new service_3.default();
        this.roleService = new service_2.default();
        this.getUserRoleList = (req, res) => {
            this.userRoleService.findUserRoleList({}, (err, userRoleListData) => {
                if (err) {
                    service_1.mongoError(err, res);
                }
                else {
                    service_1.successResponse("get userRoleListData successfull", userRoleListData, res);
                }
            });
        };
        this.getOneUserRole = (req, res) => {
            if (req.params.id) {
                const userRoleParams = { _id: req.params.id };
                this.userRoleService.findOneUserRoleAndPopulate(userRoleParams, (err, userRoleData) => {
                    if (err) {
                        return service_1.mongoError(err, res);
                    }
                    else if (!userRoleData) {
                        return service_1.notFoundResponse("User Not Found", res);
                    }
                    else {
                        service_1.successResponse("get user successfull", userRoleData, res);
                    }
                });
            }
            else {
                service_1.insufficientParameters(res);
            }
        };
        this.deleteUserRoleList = (req, res) => {
            if (req.params.id) {
                this.userRoleService.deleteUserRole(req.params.id, (err, userRoleData) => {
                    if (err) {
                        service_1.mongoError(err, res);
                    }
                    else if (userRoleData.deletedCount !== 0) {
                        service_1.successResponse("delete user successfull", null, res);
                    }
                    else {
                        service_1.failureResponse("invalid user", null, res);
                    }
                });
            }
            else {
                service_1.insufficientParameters(res);
            }
        };
        this.postUserRole = (req, res) => {
            const userParams = new schema_1.default({
                Username: req.body.Username,
                Email: req.body.Email,
                Password: bcrypt.hashSync(req.body.Password, 8),
            });
            // this.userRoleService.createUserRole(
            //   userParams,
            //   (err: Error, userRole: IUserRole) => {
            //     if (err) {
            //       return mongoError(err, res);
            //     } else {
            if (req.body.Roles.length) {
                const roleNames = req.body.Roles.map((role) => role.Name);
                const roleParams = { Name: { $in: roleNames } };
                this.roleService.findRoleList(roleParams, (err, roleListData, _) => {
                    if (err) {
                        return service_1.failureResponse("role does not exist", null, res);
                    }
                    userParams.Roles = roleListData.map((role) => role);
                    this.post(userParams, res);
                });
            }
            else {
                this.roleService.findOneRole({ Name: "user" }, (err, role) => {
                    if (err) {
                        service_1.failureResponse("role does not exist", null, res);
                    }
                    userParams.Roles = [role];
                    this.post(userParams, res);
                });
            }
            // }
            // }
            // );
        };
        this.updateUserRole = (req, res) => {
            if (req.body.Username ||
                req.body.Email ||
                req.body.Password ||
                req.body.Roles) {
                const params = { _id: req.params.id };
                this.userRoleService.findOneUserRoleAndPopulate(params, (err, userRoleData) => {
                    if (err) {
                        service_1.mongoError(err, res);
                    }
                    const userParams = new schema_1.default({
                        Username: req.body.Username
                            ? req.body.Username
                            : userRoleData.Username,
                        Email: req.body.Email ? req.body.Email : userRoleData.Email,
                        Password: req.body.Password
                            ? bcrypt.hashSync(req.body.Password, 8)
                            : userRoleData.Password,
                        _id: req.params.id,
                    });
                    if (req.body.Roles.length) {
                        const roleNames = req.body.Roles.map((role) => role.Name);
                        const roleParams = { Name: { $in: roleNames } };
                        this.roleService.findRoleList(roleParams, (err, roleListData, _) => {
                            if (err) {
                                return service_1.failureResponse("role does not exist", null, res);
                            }
                            userParams.Roles = roleListData.map((role) => role);
                            this.userRoleService.updateUserRole(userParams, (err) => {
                                if (err) {
                                    return service_1.mongoError(err, res);
                                }
                                else {
                                    return service_1.successResponse("update article successfull", null, res);
                                }
                            });
                        });
                    }
                });
            }
        };
    }
    post(userRole, res) {
        this.userRoleService.createUserRole(userRole, (err, createdUserRole) => {
            if (err) {
                return service_1.failureResponse("invalid user role", null, res);
            }
            service_1.successResponse("user role added successfully", createdUserRole, res);
        });
    }
}
exports.UserRoleController = UserRoleController;
