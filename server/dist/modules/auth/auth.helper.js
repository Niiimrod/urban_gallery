"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.moderatorBoard = exports.adminBoard = exports.userBoard = exports.allAccess = void 0;
function allAccess(req, res) {
    res.status(200).json("Public Content.");
}
exports.allAccess = allAccess;
function userBoard(req, res) {
    res.status(200).json("User Content.");
}
exports.userBoard = userBoard;
function adminBoard(req, res) {
    res.status(200).json("Admin Content.");
}
exports.adminBoard = adminBoard;
function moderatorBoard(req, res) {
    res.status(200).json("Moderator Content.");
}
exports.moderatorBoard = moderatorBoard;
