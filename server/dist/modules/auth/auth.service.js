"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const Config = require("../../config/auth.config.json");
class AuthService {
    verifyToken(token, callback) {
        jwt.verify(token, Config.secret, callback);
    }
}
exports.default = AuthService;
