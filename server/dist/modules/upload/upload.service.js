"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GridFsStorage = require("multer-gridfs-storage");
const environment_1 = require("../../environment");
class UploadService {
    constructor() {
        this.mongoUrl = `mongodb://127.0.0.1:27017/${environment_1.default.getDBName()}`;
        this.storage = new GridFsStorage({
            url: this.mongoUrl,
            options: { useNewUrlParser: true, useUnifiedTopology: true },
            file: (_, file) => {
                const match = ["image/png", "image/jpeg"];
                if (match.indexOf(file.mimetype) === -1) {
                    const filename = `${Date.now()}-urban_gallery-${file.originalname}`;
                    return filename;
                }
                return {
                    bucketName: "photos",
                    filename: `${Date.now()}-urban_gallery-${file.originalname}`,
                };
            },
        });
    }
}
exports.default = UploadService;
