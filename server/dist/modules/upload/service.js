"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const multer = require("multer");
const uuid_1 = require("uuid");
class UploadService {
    constructor() {
        this.DIR = "./uploads/images/";
        this.storage = multer.diskStorage({
            destination: (_req, _file, cb) => {
                cb(null, this.DIR);
            },
            filename: (_req, file, cb) => {
                const fileName = `${uuid_1.v4()}`;
                cb(null, fileName);
            },
        });
        this.upload = multer({
            storage: this.storage,
            fileFilter: (_req, file, cb) => {
                if (file.mimetype == "image/png" ||
                    file.mimetype == "image/jpg" ||
                    file.mimetype == "image/jpeg") {
                    cb(null, true);
                }
                else {
                    cb(null, false);
                    return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
                }
            },
        });
        this.uploadFile = (req) => {
            const url = req.protocol + "://" + req.get("host");
            if (req.files && req.files.length) {
                const reqFiles = [];
                for (var i = 0; i < req.files.length; i++) {
                    reqFiles.push(`${url}/uploads/images/${req.files[i].filename}`);
                }
                return reqFiles;
            }
            else {
                return `${url}/uploads/images/${req.file.filename}`;
            }
        };
        this.getFileNameAndFileType = (req) => {
            return {
                Name: req.file.filename,
                Type: req.file.mimetype,
            };
        };
    }
}
exports.default = UploadService;
