"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class CategoryService {
    getCategoryList(_, callback) {
        schema_1.default.find({}, callback);
    }
    createCategory(categoryParams, callback) {
        const _session = new schema_1.default(categoryParams);
        _session.save(callback);
    }
    filterCategory(query, callback) {
        schema_1.default.findOne(query, callback);
    }
    updateCategory(categoryParams, callback) {
        const query = { _id: categoryParams._id };
        schema_1.default.findOneAndUpdate(query, categoryParams, callback);
    }
    deleteCategory(_id, callback) {
        const query = { _id: _id };
        schema_1.default.findByIdAndDelete(query, callback);
    }
}
exports.default = CategoryService;
