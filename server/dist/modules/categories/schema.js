"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    CategoryName: String,
    CategoryDescription: String,
    CategoryPhoto: String,
    CategoryPhotoList: [
        {
            type: Schema.Types.ObjectId,
            ref: "photos",
        },
    ],
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "categories",
});
const Category = mongoose.model("categories", schema);
exports.default = Category;
