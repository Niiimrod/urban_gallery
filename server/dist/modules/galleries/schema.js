"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    GalleryName: String,
    GalleryDescription: String,
    GalleryPhoto: {
        type: Schema.Types.ObjectId,
        ref: "images",
    },
    GalleryImageList: [
        {
            type: Schema.Types.ObjectId,
            ref: "images",
        },
    ],
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "galleries",
});
const Gallery = mongoose.model("galleries", schema);
exports.default = Gallery;
