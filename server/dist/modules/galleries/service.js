"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class GalleryService {
    getGalleryList(query) {
        return schema_1.default.find(query)
            .populate([
            {
                path: "GalleryPhoto",
            },
            {
                path: "GalleryImageList",
            },
        ])
            .exec();
    }
    createGallery(galleryParams) {
        const gallery = new schema_1.default(galleryParams);
        return gallery.save();
    }
    filterGallery(query) {
        return schema_1.default.findOne(query)
            .populate([
            {
                path: "GalleryPhoto",
            },
            {
                path: "GalleryImageList",
            },
        ])
            .exec();
    }
    updateGallery(galleryParams) {
        console.log("galleryParams", galleryParams);
        const query = { _id: galleryParams._id };
        return schema_1.default.findOneAndUpdate(query, galleryParams).exec();
    }
    deleteGallery(_id, callback) {
        const query = { _id: _id };
        schema_1.default.findByIdAndDelete(query, callback);
    }
}
exports.default = GalleryService;
