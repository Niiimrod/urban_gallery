"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class PhotoService {
    getPhotoList(_, callback) {
        schema_1.default.find({}, callback);
    }
    createPhoto(photoParams, callback) {
        const _session = new schema_1.default(photoParams);
        _session.save(callback);
    }
    filterPhoto(query, callback) {
        schema_1.default.findOne(query, callback);
    }
    updatePhoto(photoParams, callback) {
        const query = { _id: photoParams._id };
        schema_1.default.findOneAndUpdate(query, photoParams, callback);
    }
    deletePhoto(_id, callback) {
        const query = { _id: _id };
        schema_1.default.findByIdAndDelete(query, callback);
    }
}
exports.default = PhotoService;
