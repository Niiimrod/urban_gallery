"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    PhotoName: String,
    PhotoUrl: String,
    PhotoDescription: String,
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "photos",
});
const Photo = mongoose.model("photos", schema);
exports.default = Photo;
