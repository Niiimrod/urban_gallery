"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class UserRoleService {
    createUserRole(userRole, callback) {
        const _session = new schema_1.default(userRole);
        _session.save(callback);
    }
    findOneUserRole(query, callback) {
        schema_1.default.findOne(query, callback);
    }
    findOneUserRoleAndPopulate(query, callback) {
        schema_1.default.findOne(query).populate("Roles", "-__v").exec(callback);
    }
    updateUserRole(userRoleParams, callback) {
        const query = { _id: userRoleParams._id };
        schema_1.default.findOneAndUpdate(query, userRoleParams, callback);
    }
    findUserRoleList(query, callback) {
        schema_1.default.find(query).populate("Roles", "-__v").exec(callback);
    }
    deleteUserRole(_id, callback) {
        const query = { _id: _id };
        schema_1.default.findByIdAndDelete(query, callback);
    }
}
exports.default = UserRoleService;
