"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
    Username: String,
    Email: String,
    Password: String,
    Roles: [{
            type: Schema.Types.ObjectId,
            ref: "roles"
        }]
}, {
    collection: "user-roles",
});
const UserRole = mongoose.model("userRoles", schema);
exports.default = UserRole;
