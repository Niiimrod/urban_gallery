"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class ArticleService {
    getArticleList(_, callback) {
        schema_1.default.find({}, callback);
    }
    createArticle(articleParams, callback) {
        const _session = new schema_1.default(articleParams);
        _session.save(callback);
    }
    filterArticle(query, callback) {
        schema_1.default.findOne(query, callback);
    }
    updateArticle(articleParams, callback) {
        const query = { _id: articleParams._id };
        schema_1.default.findOneAndUpdate(query, articleParams, callback);
    }
    deleteArticle(_id, callback) {
        const query = { _id: _id };
        schema_1.default.findByIdAndDelete(query, callback);
    }
}
exports.default = ArticleService;
