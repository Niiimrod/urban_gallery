"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    Name: String,
    User: String,
    Title: String,
    Description: String,
    IsPublished: Boolean,
    ImgSrc: String,
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "articles",
});
const Article = mongoose.model("articles", schema);
exports.default = Article;
