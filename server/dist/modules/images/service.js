"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class ImageService {
    createImage(imageParams) {
        const image = new schema_1.default(imageParams);
        return image.save();
    }
    findOneImage(query) {
        return schema_1.default.findOne(query).exec();
    }
    findImageList(query) {
        return schema_1.default.find({
            _id: query,
        }).exec();
    }
}
exports.default = ImageService;
