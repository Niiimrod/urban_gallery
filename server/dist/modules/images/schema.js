"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    Img: {
        required: true,
        type: String,
    },
    Name: {
        required: true,
        type: String,
    },
    Alt: {
        required: true,
        type: String
    },
    Title: {
        required: true,
        type: String
    },
    Type: {
        required: true,
        type: String,
    },
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "images",
});
const Image = mongoose.model("images", schema);
exports.default = Image;
