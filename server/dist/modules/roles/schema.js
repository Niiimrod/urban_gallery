"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
    Name: String
}, {
    collection: "roles",
});
const Role = mongoose.model("roles", schema);
exports.default = Role;
