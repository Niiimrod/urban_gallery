"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class RoleService {
    findRoleList(query) {
        return schema_1.default.find(query).exec();
    }
    findOneRole(query) {
        return schema_1.default.findOne(query).exec();
    }
}
exports.default = RoleService;
