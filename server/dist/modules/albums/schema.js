"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    AlbumName: String,
    AlbumDescription: String,
    AlbumPhoto: {
        type: Schema.Types.ObjectId,
        ref: "images",
    },
    AlbumGalleries: [
        {
            type: Schema.Types.ObjectId,
            ref: "galleries",
        },
    ],
    AlbumIsPublished: Boolean,
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "albums",
});
const Album = mongoose.model("albums", schema);
exports.default = Album;
