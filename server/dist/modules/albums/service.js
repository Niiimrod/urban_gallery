"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class AlbumService {
    getAlbumList(query) {
        return schema_1.default.find(query)
            .populate([
            {
                path: "AlbumPhoto",
            },
            {
                path: "AlbumGalleries",
                populate: {
                    path: "GalleryPhoto GalleryImageList",
                },
            },
        ])
            .exec();
    }
    createAlbum(albumParams) {
        const album = new schema_1.default(albumParams);
        return album.save();
    }
    filterAlbum(query) {
        return schema_1.default.findOne(query)
            .populate([
            {
                path: "AlbumPhoto",
            },
            {
                path: "AlbumGalleries",
                populate: {
                    path: "GalleryPhoto GalleryImageList",
                },
            },
        ])
            .exec();
    }
    updateAlbum(albumParams, callback) {
        const query = { _id: albumParams._id };
        schema_1.default.findOneAndUpdate(query, albumParams, callback);
    }
    deleteAlbum(_id) {
        const query = { _id: _id };
        // Album.findByIdAndDelete(query, callback);
        return schema_1.default.remove(query).exec();
    }
    addGalleryToAlbum(albumId, galleryParams) {
        return schema_1.default.findByIdAndUpdate(albumId, {
            $push: { AlbumGalleries: galleryParams._id },
        }).exec();
    }
}
exports.default = AlbumService;
