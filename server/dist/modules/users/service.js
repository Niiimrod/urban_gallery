"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
class UserService {
    createUser(userParams) {
        const user = new schema_1.default(userParams);
        return user.save();
    }
    findOneUser(query) {
        return schema_1.default.findOne(query).exec();
    }
    findOneUserAndPopulate(query) {
        return schema_1.default.findOne(query).populate("Roles", "-__v").exec();
    }
    updateUser(userParams) {
        const query = { _id: userParams._id };
        return schema_1.default.findOneAndUpdate(query, userParams).exec();
    }
    findUserList(query) {
        return schema_1.default.find(query).populate("Roles", "-__v").exec();
    }
    deleteUser(_id) {
        const query = { _id: _id };
        return schema_1.default.findByIdAndDelete(query).exec();
    }
}
exports.default = UserService;
