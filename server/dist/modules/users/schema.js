"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    Username: { type: String, required: true },
    Email: { type: String, required: true },
    Password: { type: String, required: true },
    Roles: [
        {
            type: Schema.Types.ObjectId,
            ref: "roles",
            required: true
        },
    ],
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "users",
});
const User = mongoose.model("users", schema);
exports.default = User;
