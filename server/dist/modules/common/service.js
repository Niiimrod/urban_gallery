"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getModificationNote = exports.notFoundResponse = exports.roleRequiredResponse = exports.unauthorizedTokenResponse = exports.unauthorizedResponse = exports.tokenFailureResponse = exports.roleDoesNotExist = exports.userAlreadyExist = exports.mongoError = exports.insufficientParameters = exports.failureResponse = exports.successResponse = void 0;
const model_1 = require("./model");
function successResponse(message, DATA, res) {
    res.status(model_1.response_status_codes.success).json({
        STATUS: "SUCCESS",
        MESSAGE: message,
        DATA,
    });
}
exports.successResponse = successResponse;
function failureResponse(message, DATA, res) {
    res.status(model_1.response_status_codes.bad_request).json({
        STATUS: "FAILURE",
        MESSAGE: message,
        DATA,
    });
}
exports.failureResponse = failureResponse;
function insufficientParameters(res) {
    res.status(model_1.response_status_codes.bad_request).json({
        STATUS: "FAILURE",
        MESSAGE: "Insufficient parameters",
        DATA: {},
    });
}
exports.insufficientParameters = insufficientParameters;
function mongoError(err, res) {
    res.status(model_1.response_status_codes.bad_request).json({
        STATUS: "FAILURE",
        MESSAGE: "MongoDB error",
        DATA: err,
    });
}
exports.mongoError = mongoError;
function userAlreadyExist(res) {
    res.status(model_1.response_status_codes.bad_request).json({
        STATUS: "FAILURE",
        MESSAGE: "User already exist",
        DATA: {},
    });
}
exports.userAlreadyExist = userAlreadyExist;
function roleDoesNotExist(message, res) {
    res.status(model_1.response_status_codes.bad_request).json({
        STATUS: "FAILURE",
        MESSAGE: message,
        DATA: {},
    });
}
exports.roleDoesNotExist = roleDoesNotExist;
function tokenFailureResponse(res) {
    res.status(model_1.response_status_codes.token_error).json({
        STATUS: "FAILURE",
        MESSAGE: "No token provided",
        DATA: {},
    });
}
exports.tokenFailureResponse = tokenFailureResponse;
function unauthorizedResponse(res) {
    res.status(model_1.response_status_codes.unauthorized).json({
        STATUS: "UNAUTHORIZED",
        MESSAGE: "Unauthorized!",
        DATA: {},
    });
}
exports.unauthorizedResponse = unauthorizedResponse;
function unauthorizedTokenResponse(res) {
    res.status(model_1.response_status_codes.unauthorized).json({
        STATUS: "UNAUTHORIZED",
        MESSAGE: "Unauthorized!",
        DATA: {
            accessToken: null,
            message: "Invalid Password!",
        },
    });
}
exports.unauthorizedTokenResponse = unauthorizedTokenResponse;
function roleRequiredResponse(message, res) {
    res.status(model_1.response_status_codes.token_error).json({
        STATUS: "ROLE_REQUIRED",
        MESSAGE: message,
        DATA: {},
    });
}
exports.roleRequiredResponse = roleRequiredResponse;
function notFoundResponse(message, res) {
    res.status(model_1.response_status_codes.not_found).json({
        STATUS: "NOT_FOUND",
        MESSAGE: message,
        DATA: {},
    });
}
exports.notFoundResponse = notFoundResponse;
function getModificationNote(message) {
    return {
        ModificationDate: new Date(Date.now()),
        ModificationNote: message,
    };
}
exports.getModificationNote = getModificationNote;
