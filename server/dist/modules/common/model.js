"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Roles = exports.RoleList = exports.response_status_codes = exports.ModificationNoteSchema = void 0;
const mongoose = require("mongoose");
exports.ModificationNoteSchema = new mongoose.Schema({
    ModificationDate: Date,
    ModificationNote: String,
});
var response_status_codes;
(function (response_status_codes) {
    response_status_codes[response_status_codes["success"] = 200] = "success";
    response_status_codes[response_status_codes["bad_request"] = 400] = "bad_request";
    response_status_codes[response_status_codes["internal_server_error"] = 500] = "internal_server_error";
    response_status_codes[response_status_codes["token_error"] = 403] = "token_error";
    response_status_codes[response_status_codes["unauthorized"] = 401] = "unauthorized";
    response_status_codes[response_status_codes["not_found"] = 404] = "not_found";
})(response_status_codes = exports.response_status_codes || (exports.response_status_codes = {}));
exports.RoleList = ["user", "admin", "moderator", "artist"];
var Roles;
(function (Roles) {
    Roles["user"] = "user";
    Roles["admin"] = "admin";
    Roles["moderator"] = "moderator";
    Roles["artist"] = "artist";
})(Roles = exports.Roles || (exports.Roles = {}));
