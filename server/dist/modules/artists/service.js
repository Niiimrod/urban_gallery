"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("./schema");
// .populate({
//   path: 'friends',
//   // Get friends of friends - populate the 'friends' array for every friend
//   populate: { path: 'friends' }
// });
class ArtistService {
    getArtistList(query) {
        return __awaiter(this, void 0, void 0, function* () {
            return schema_1.default.find(query)
                .populate([
                {
                    path: "ArtistAlbums",
                    populate: {
                        path: "AlbumPhoto",
                    },
                },
                {
                    path: "ArtistAlbums",
                    populate: {
                        path: "AlbumGalleries",
                        populate: {
                            path: "GalleryImageList GalleryPhoto",
                        },
                    },
                },
                {
                    path: "ArtistRoles",
                },
                {
                    path: "ArtistPhoto",
                },
            ])
                .exec();
            // return Artist.find(query).populate("ArtistRoles ArtistAlbums ArtistPhoto").exec();
        });
    }
    createArtist(artistParams) {
        const artist = new schema_1.default(artistParams);
        return artist.save();
    }
    filterArtist(query) {
        return schema_1.default.findOne(query).exec();
    }
    updateArtist(artistParams) {
        const query = { _id: artistParams._id };
        return schema_1.default.findOneAndUpdate(query, artistParams).exec();
    }
    addAlbumToArtist(artistId, albumParams) {
        return schema_1.default.findByIdAndUpdate(artistId, {
            $push: { ArtistAlbums: albumParams._id },
        }).exec();
    }
    deleteArtist(_id) {
        const query = { _id: _id };
        return schema_1.default.findByIdAndDelete(query).exec();
    }
    findOneArtistAndPopulate(query) {
        return schema_1.default.findOne(query)
            .populate([
            {
                path: "ArtistAlbums",
                populate: {
                    path: "AlbumPhoto",
                },
            },
            {
                path: "ArtistAlbums",
                populate: {
                    path: "AlbumGalleries",
                    populate: {
                        path: "GalleryImageList GalleryPhoto",
                    },
                },
            },
            {
                path: "ArtistRoles",
            },
            {
                path: "ArtistPhoto",
            },
        ])
            .exec();
    }
}
exports.default = ArtistService;
