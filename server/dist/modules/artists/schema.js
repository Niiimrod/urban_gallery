"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    ArtistFirstname: { type: String, required: true },
    ArtistLastname: { type: String, required: true },
    ArtistName: { type: String, required: true },
    ArtistEmail: { type: String, required: true },
    ArtistWebsite: { type: String, required: true },
    ArtistDescription: { type: String, required: true },
    ArtistPhoto: {
        type: Schema.Types.ObjectId,
        ref: "images",
    },
    ArtistPassword: { type: String, required: true },
    ArtistRoles: [
        {
            type: Schema.Types.ObjectId,
            ref: "roles",
        },
    ],
    ArtistAlbums: [
        {
            type: Schema.Types.ObjectId,
            ref: "albums",
        },
    ],
    ModificationNotes: [model_1.ModificationNoteSchema],
}, {
    collection: "artists",
});
const Artist = mongoose.model("artists", schema);
exports.default = Artist;
