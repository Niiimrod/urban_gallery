"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");
const environment_1 = require("../environment");
const schema_1 = require("../modules/roles/schema");
const album_routes_1 = require("../routes/album_routes");
const auth_routes_1 = require("../routes/auth_routes");
const common_routes_1 = require("../routes/common_routes");
const gallery_routes_1 = require("../routes/gallery_routes");
const upload_routes_1 = require("../routes/upload_routes");
const user_routes_1 = require("../routes/user_routes");
class App {
    constructor() {
        this.mongoUrl = `mongodb://127.0.0.1:27017/${environment_1.default.getDBName()}`;
        this.user_routes = new user_routes_1.UserRoutes();
        this.auth_routes = new auth_routes_1.AuthRoutes();
        this.upload_routes = new upload_routes_1.UploadRoutes();
        this.album_routes = new album_routes_1.AlbumRoutes();
        this.gallery_routes = new gallery_routes_1.GalleryRoutes();
        this.common_routes = new common_routes_1.CommonRoutes();
        this.app = express();
        this.loadConfig();
        this.loadMongoSetup();
        this.user_routes.route(this.app);
        this.auth_routes.route(this.app);
        this.upload_routes.route(this.app);
        this.album_routes.route(this.app);
        this.gallery_routes.route(this.app);
        //keep it last routes
        this.common_routes.route(this.app);
    }
    loadConfig() {
        this.app.use("/uploads", express.static(process.cwd() + "/uploads"));
        this.app.use(cors());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
    }
    loadMongoSetup() {
        mongoose.connect(this.mongoUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        }, () => {
            this.initial();
        });
    }
    initial() {
        schema_1.default.estimatedDocumentCount({}, (err, count) => {
            if (!err && count === 0) {
                new schema_1.default({
                    Name: "user",
                }).save((err) => {
                    if (err) {
                        console.log("error", err);
                    }
                    console.log("added 'user' to roles collection");
                });
                new schema_1.default({
                    Name: "moderator",
                }).save((err) => {
                    if (err) {
                        console.log("error", err);
                    }
                    console.log("added 'moderator' to roles collection");
                });
                new schema_1.default({
                    Name: "admin",
                }).save((err) => {
                    if (err) {
                        console.log("error", err);
                    }
                    console.log("added 'admin' to roles collection");
                });
            }
        });
    }
}
exports.default = new App().app;
