import {
  failureResponse,
  getModificationNote,
  mongoError,
  successResponse,
} from "../modules/common/service";
import Image from "../modules/images/schema";
import ImageService from "../modules/images/service";
import UploadService from "../modules/upload/service";

export class UploadController {
  private uploadService: UploadService = new UploadService();
  private imageService: ImageService = new ImageService();

  public postImage = async (req, res) => {
    const file = this.uploadService.uploadFile(req);
    const nameAndType = this.uploadService.getFileNameAndFileType(req);
    const imageParams = new Image({
      Img: file,
      Name: nameAndType.Name,
      Type: nameAndType.Type,
      Alt: String(req.body.alt),
      Title: String(req.body.title),
      ModificationNotes: getModificationNote("Image Created"),
    });
    
    try {
      const image = await this.imageService.createImage(imageParams);
      if (!image) {
        return failureResponse("image fail", null, res);
      }
      return successResponse("image added successfully", image, res);
    } catch (error) {
      return mongoError(error, res);
    }
  };

  public postImageList = async (req, res) => {
    const files = this.uploadService.uploadFile(req);
    let promiseArray = [];
    for (var i = 0; i < req.files.length; i++) {
      const artistParams = new Image({
        Img: files[i],
        Name: req.files[i].filename,
        Type: req.files[i].mimetype,
        ModificationNotes: getModificationNote("Image Created"),
      });
      promiseArray.push(this.imageService.createImage(artistParams));
    }

    Promise.all(promiseArray)
      .then((imageList) => {
        return successResponse("image added successfully", imageList, res);
      })
      .catch((error) => {
        return mongoError(error, res);
      });
  };
}
