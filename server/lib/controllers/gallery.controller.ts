import { Request, Response } from "express";
import AlbumService from "../modules/albums/service";
import {
  failureResponse,
  getModificationNote,
  insufficientParameters,
  mongoError,
  notFoundResponse,
  successResponse,
} from "../modules/common/service";
import Gallery from "../modules/galleries/schema";
import GalleryService from "../modules/galleries/service";
import ImageService from "../modules/images/service";

export class GalleryController {
  private albumService: AlbumService = new AlbumService();
  private imageService: ImageService = new ImageService();
  private galleryService: GalleryService = new GalleryService();

  public postOneAlbumGallery = async (req: Request, res: Response) => {
    if (req.params.albumId) {
      const galleryParams = new Gallery({
        GalleryName: req.body.GalleryName,
        GalleryDescription: req.body.GalleryDescription,
        GalleryPhoto: req.body.GalleryPhoto,
        GalleryImageList: req.body.GalleryImageList,
        ModificationNotes: getModificationNote("Gallery Created"),
      });

      try {
        const galleryPhoto = await this.imageService.findOneImage({
          _id: req.body.GalleryPhoto._id,
        });

        if (!galleryPhoto) {
          return failureResponse("image does not exist", null, res);
        }

        // galleryParams.GalleryPhoto = galleryPhoto;

        // const galleryImageList = await this.imageService.findImageList({
        //   $in: req.body.GalleryImageList.map(
        //     (galleryImage: IImage) => galleryImage._id
        //   ),
        // });

        // if (!galleryImageList) {
        //   return failureResponse("galleryImageList does not exist", null, res);
        // }

        // galleryParams.GalleryImageList = galleryImageList;

        const galleryCreate = await this.galleryService.createGallery(
          galleryParams
        );
        if (!galleryCreate) {
          return failureResponse("invalid album response", null, res);
        }

        const albumUpdate = await this.albumService.addGalleryToAlbum(
          req.params.albumId,
          galleryCreate
        );

        if (!albumUpdate) {
          return failureResponse("invalid albumUpdate response", null, res);
        }
        return successResponse("album added successfully", albumUpdate, res);
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };

  public putGallery = async (req: Request, res: Response) => {
    if (req.params.albumId) {
      let currentGallery;
      try {
        const params = { _id: req.params.galleryId };
        currentGallery = await this.galleryService.filterGallery(params);
        const galleryParams = new Gallery({
          GalleryName: req.body.GalleryName
            ? req.body.GalleryName
            : currentGallery.GalleryName,
          GalleryDescription: req.body.GalleryDescription
            ? req.body.GalleryDescription
            : currentGallery.GalleryDescription,
          GalleryPhoto: req.body.GalleryPhoto
            ? req.body.GalleryPhoto
            : currentGallery.GalleryPhoto,
          GalleryImageList: req.body.GalleryImageList
            ? req.body.GalleryImageList
            : currentGallery.GalleryImageList,
          _id: req.params.galleryId,
          ModificationNotes: getModificationNote("Gallery Updated"),
        });
        const updateGallery = await this.galleryService.updateGallery(
          galleryParams
        );
        if (!updateGallery) {
          return failureResponse("gallery update failled", null, res);
        }
        return successResponse(
          "update gallery successfull",
          null,
          res
        );
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };

  public getAlbumGalleryList = async (_req: Request, res: Response) => {
    try {
      const galleryList = await this.galleryService.getGalleryList({});
      return successResponse("get galleryList successfull", galleryList, res);
    } catch (error) {
      return mongoError(error, res);
    }
  };

  public getOneGallery = async (req: Request, res: Response) => {
    if (req.params.galleryId) {
      try {
        const gallery = await this.galleryService.filterGallery({
          _id: req.params.galleryId,
        });
        if (!gallery) {
          return notFoundResponse("Gallery Not Found", res);
        }
        return successResponse("get gallery successfull", gallery, res);
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };
}
