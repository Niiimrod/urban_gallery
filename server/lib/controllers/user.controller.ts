import * as bcrypt from "bcryptjs";
import { Request, Response } from "express";
import {
  failureResponse,
  getModificationNote,
  insufficientParameters,
  mongoError,
  notFoundResponse,
  successResponse,
} from "../modules/common/service";
import { IRole } from "../modules/roles/model";
import RoleService from "../modules/roles/service";
import { IUser } from "../modules/users/model";
import User from "../modules/users/schema";
import UserService from "../modules/users/service";

export class UserController {
  private userService: UserService = new UserService();
  private roleService: RoleService = new RoleService();

  public getUserList = async (_req: Request, res: Response) => {
    try {
      const userList = await this.userService.findUserList({});
      if (!userList) {
        return failureResponse("can't load roleList", null, res);
      }
      return successResponse("get userListResponse successfull", userList, res);
    } catch (error) {
      return mongoError(error, res);
    }
  };

  public getOneUser = async (req: Request, res: Response) => {
    if (req.params.id) {
      try {
        const userFind = await this.userService.findOneUserAndPopulate({
          _id: req.params.id,
        });
        if (!userFind) {
          return notFoundResponse("User Not Found", res);
        }
        return successResponse("get user successfull", userFind, res);
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };

  public deleteUser = async (req: Request, res: Response) => {
    if (req.params.id) {
      try {
        const userDeleted = await this.userService.deleteUser(req.params.id);
        if (userDeleted === null) {
          return failureResponse("invalid user", null, res);
        }
        return successResponse("delete user successfull", null, res);
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };

  private async createUser(user: IUser, res: Response) {
    let createUser: IUser;
    try {
      createUser = await this.userService.createUser(user);
      if (!createUser) {
        return failureResponse("invalid user role", null, res);
      }
      return successResponse("user role added successfully", createUser, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public postUser = async (req: Request, res: Response) => {
    const userParams = new User({
      Username: req.body.Username,
      Email: req.body.Email,
      Password: bcrypt.hashSync(req.body.Password, 8),
      ModificationNotes: getModificationNote("User Created"),
    });

    if (req.body.Roles.length) {
      const roleNames = req.body.Roles.map((role: IRole) => role.Name);
      try {
        const roleList = await this.roleService.findRoleList({
          Name: { $in: roleNames },
        });
        if (!roleList) {
          return failureResponse("can't find any roles", null, res);
        }
        userParams.Roles = roleList.map((role: IRole) => role);
        this.createUser(userParams, res);
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      try {
        const role = await this.roleService.findOneRole({ Name: "user" });
        if (!role) {
          return failureResponse("role does not exist", null, res);
        }
        userParams.Roles = [role];
        this.createUser(userParams, res);
      } catch (error) {
        return mongoError(error, res);
      }
    }
  };

  public putUser = async (req: Request, res: Response) => {
    if (
      req.body.Username ||
      req.body.Email ||
      req.body.Password ||
      req.body.Roles
    ) {
      let user: IUser;

      try {
        user = await this.userService.findOneUserAndPopulate({
          _id: req.params.id,
        });
        if (!user) {
          return failureResponse("user does not exist", null, res);
        }

        user.ModificationNotes.push(getModificationNote("User Updated"));
        const newUser = new User({
          Username: req.body.Username ? req.body.Username : user.Username,
          Email: req.body.Email ? req.body.Email : user.Email,
          Password: req.body.Password
            ? bcrypt.hashSync(req.body.Password, 8)
            : user.Password,
          _id: req.params.id,
          ModificationNotes: user.ModificationNotes,
        });

        if (req.body.Roles.length) {
          const roleNames = req.body.Roles.map((role: IRole) => role.Name);
          const roleList = await this.roleService.findRoleList({
            Name: { $in: roleNames },
          });
          if (!roleList) {
            return failureResponse("can't load roleList", null, res);
          }
          newUser.Roles = roleList.map((role: IRole) => role);
          this.updateUser(newUser, res);
        } else {
          const role = await this.roleService.findOneRole({ Name: "user" });
          if (!role) {
            return failureResponse("role does not exist", null, res);
          }
          newUser.Roles = [role];
          this.updateUser(newUser, res);
        }
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };

  private async updateUser(user: IUser, res: Response) {
    try {
      const userUpdated = await this.userService.updateUser(user);
      if (!userUpdated) {
        return failureResponse("can't update user", null, res);
      }

      const populatedUser = await this.userService.findOneUserAndPopulate({
        _id: userUpdated._id,
      });
      if (!populatedUser) {
        return failureResponse("can't find user", null, res);
      }
      return successResponse("user updated successfully", populatedUser, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }
}
