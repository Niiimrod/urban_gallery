import { NextFunction, Request, Response } from "express";
import AuthService from "../modules/auth/auth.service";
import { mongoError, tokenFailureResponse } from "../modules/common/service";

export class TokenController {
  private authService: AuthService = new AuthService();

  public verifyToken = (req: Request, res: Response, next: NextFunction): void | NextFunction => {
    let token = req.headers["x-access-token"];
    if (!token) {
      return tokenFailureResponse(res);
    }

    this.authService.verifyToken(token, (err: Error, decoded: any) => {
      if (err) {
        return mongoError(err, res);
      } else {
        req.params.UserId = decoded.id;
        next();
      }
    });
  };
}
