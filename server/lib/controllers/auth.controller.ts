import * as bcrypt from "bcryptjs";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import * as Config from "../config/auth.config.json";
import { RoleList, Roles } from "../modules/common/model";
import {
  failureResponse,
  getModificationNote,
  mongoError,
  notFoundResponse,
  roleDoesNotExist,
  roleRequiredResponse,
  successResponse,
  unauthorizedTokenResponse,
  userAlreadyExist,
} from "../modules/common/service";
import RoleService from "../modules/roles/service";
import { IUser } from "../modules/users/model";
import User from "../modules/users/schema";
import UserService from "../modules/users/service";

export class AuthController {
  private userService: UserService = new UserService();
  private roleService: RoleService = new RoleService();

  public checkDuplicateUsernameOrEmail = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const userNameParams = { Username: req.body.Username };
    const userEmailParams = { Email: req.body.Email };
    try {
      const userNameExist = await this.userService.findOneUser(userNameParams);
      const userEmailExist = await this.userService.findOneUser(
        userEmailParams
      );
      if (userNameExist || userEmailExist) {
        return userAlreadyExist(res);
      }
    } catch (error) {
      return mongoError(error, res);
    }
    next();
  };

  public checkRolesExisted = (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    if (req.body.Roles) {
      for (let i = 0; i < req.body.Roles.length; i++) {
        if (!RoleList.includes(req.body.Roles[i].Name)) {
          roleDoesNotExist(
            `Failed! Role ${req.body.Roles[i].Name} does not exist!`,
            res
          );
          return;
        }
      }
    }
    next();
  };

  public signUp = async (req: Request, res: Response) => {
    const userParams = new User({
      Username: req.body.Username,
      Email: req.body.Email,
      Password: bcrypt.hashSync(req.body.Password, 8),
      ModificationNotes: getModificationNote("User Created"),
    });
    try {
      const role = await this.roleService.findOneRole({ Name: "user" });
      if (!role) {
        return failureResponse("role does not exist", null, res);
      }
      userParams.Roles = [role];
      const userCreate = await this.userService.createUser(userParams);
      if (!userCreate) {
        return failureResponse("can't create user", null, res);
      }
      return successResponse("user added successfully", userCreate, res);
    } catch (error) {
      return mongoError(error, res);
    }
  };

  public isTokenValid = async (_: Request, res: Response) => {
    return successResponse("user added successfully", null, res);
  }

  public signIn = async (req: Request, res: Response) => {
    let userFind: IUser;
    try {
      const signinParams = { Email: req.body.Email };
      userFind = await this.userService.findOneUserAndPopulate(signinParams);
    } catch (error) {
      return mongoError(error, res);
    }
    if (userFind) {
      return this.signinUser(req, res, userFind);
    } 
  };

  private signinUser(
    req: Request,
    res: Response,
    params: any,
  ) {
    const passwordIsValid = bcrypt.compareSync(
      req.body.Password,
      params.Password
    );

    if (!passwordIsValid) {
      return unauthorizedTokenResponse(res);
    }

    const token = jwt.sign({ id: params.id }, Config.secret, {
      expiresIn: 86400, // 24 hours
    });

    return successResponse(
      "signin succesfully",
      {
        _id: params.id,
        Username: params.Username,
        Roles:params.Roles,
        AccessToken: token,
      },
      res
    );
  }

  public isAdmin = (req: Request, res: Response, next: NextFunction) => {
    this.userHasRole(req, res, next, Roles.admin);
  };

  public isModerator = (req: Request, res: Response, next: NextFunction) => {
    this.userHasRole(req, res, next, Roles.moderator);
  };

  private async userHasRole(
    req: Request,
    res: Response,
    next: NextFunction,
    roleName: string
  ) {
    try {
      const user = await this.userService.findOneUser({
        _id: req.params.UserId,
      });
      if (!user) {
        return notFoundResponse("User not found", res);
      }
      const roleList = await this.roleService.findRoleList({
        _id: { $in: user.Roles },
      });
      if (!roleList) {
        return notFoundResponse("Can't load roleList", res);
      }
      for (let i = 0; i < roleList.length; i++) {
        if (roleList[i].Name === roleName) {
          next();
        } else {
          return roleRequiredResponse(`Require ${roleName} Role!`, res);
        }
      }
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public isAdminOrModerator = (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    this.userHasRoles(req, res, next, [Roles.moderator, Roles.admin]);
  };


  private async userHasRoles(
    req: Request,
    res: Response,
    next: NextFunction,
    roleNameList: string[]
  ) {
    try {
      const user = await this.userService.findOneUser({
        _id: req.params.UserId,
      });
      if (!user) {
        return notFoundResponse("User not found", res);
      }
      const roleList = await this.roleService.findRoleList({
        _id: { $in: user.Roles },
      });
      for (let i = 0; i < roleList.length; i++) {
        if (roleNameList.some((roleName) => roleName === roleList[i].Name)) {
          next();
          return;
        }
      }
    } catch (error) {
      return mongoError(error, res);
    }

    return roleRequiredResponse(`Require Admin or Moderator Roles!`, res);
  }
}
