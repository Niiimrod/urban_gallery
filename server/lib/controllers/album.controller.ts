import { Request, Response } from "express";
import Album from "../modules/albums/schema";
import AlbumService from "../modules/albums/service";
import {
  failureResponse,
  getModificationNote,
  insufficientParameters,
  mongoError,
  notFoundResponse,
  successResponse,
} from "../modules/common/service";
import ImageService from "../modules/images/service";
export class AlbumController {
  private albumService: AlbumService = new AlbumService();
  private imageService: ImageService = new ImageService();

  public postOneAlbum = async (req: Request, res: Response) => {
    const albumParams = new Album({
      AlbumName: req.body.AlbumName,
      AlbumDescription: req.body.AlbumDescription,
      AlbumPhoto: req.body.AlbumPhoto,
      AlbumGalleries: req.params.AlbumGalleries,
      AlbumIsPublished: req.body.AlbumIsPublished,
      ModificationNotes: getModificationNote("Album Created"),
    });

    try {
      const albumImage = await this.imageService.findOneImage({
        _id: req.body.AlbumPhoto._id,
      });
      if (!albumImage) {
        return failureResponse("image does not exist", null, res);
      }
      albumParams.AlbumPhoto = albumImage;

      const albumCreate = await this.albumService.createAlbum(albumParams);
      if (!albumCreate) {
        return failureResponse("invalid album response", null, res);
      }
      return successResponse("album added successfully", albumCreate, res);
    } catch (error) {
      return mongoError(error, res);
    }
  };

  public getOneAlbum = async (req: Request, res: Response) => {
    if (req.params.albumId) {
      try {
        const album = await this.albumService.filterAlbum({
          _id: req.params.albumId,
        });
        if (!album) {
          return notFoundResponse("Album Not Found", res);
        }
        return successResponse("get album successfull", album, res);
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };

  public getAlbumList = async (_req: Request, res: Response) => {
    try {
      const albumList = await this.albumService.getAlbumList({});
      return successResponse("get albumList successfull", albumList, res);
    } catch (error) {
      return mongoError(error, res);
    }
  };


  public deleteAlbum = async (req: Request, res: Response) => {
    if (req.params.albumId) {
      try {
        const albumDeleted = await this.albumService.deleteAlbum(req.params.albumId);
        if (albumDeleted === null) {
          return failureResponse("invalid album", null, res);
        }
        return successResponse("delete album successfull", null, res);
      } catch (error) {
        return mongoError(error, res);
      }
    } else {
      return insufficientParameters(res);
    }
  };  
}
