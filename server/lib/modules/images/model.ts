import { ModificationNote } from "modules/common/model";
import * as mongoose from "mongoose";

export interface IImage extends mongoose.Document {
  _id?: string;
  Img: string;
  Name: string;
  Type: string;
  Alt: string;
  Title: string;
  ModificationNotes: ModificationNote[];
}
