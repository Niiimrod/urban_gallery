import * as mongoose from "mongoose";
import { ModificationNoteSchema } from "../common/model";
import { IImage } from "./model";

const Schema = mongoose.Schema;
const schema = new Schema(
  {
    Img: {
      required: true,
      type: String,
    },
    Name: {
      required: true,
      type: String,
    },
    Alt: {
      required: true,
      type: String
    },
    Title: {
      required: true,
      type: String
    },    
    Type: {
      required: true,
      type: String,
    },
    ModificationNotes: [ModificationNoteSchema],
  },
  {
    collection: "images",
  }
);
const Image = mongoose.model<IImage>("images", schema);
export default Image;
