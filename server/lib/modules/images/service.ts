import { IImage } from "./model";
import Image from "./schema";

export default class ImageService {
  public createImage(imageParams: IImage): Promise<IImage> {
    const image = new Image(imageParams);
    return image.save();
  }

  public findOneImage(query: any): Promise<IImage> {
    return Image.findOne(query).exec();
  }

  public findImageList(query: any): Promise<IImage[]> {
    return Image.find({
      _id: query,
    }).exec();
  }
}
