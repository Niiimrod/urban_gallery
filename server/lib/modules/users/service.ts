import { IUser } from "./model";
import User from "./schema";

export default class UserService {
  public createUser(userParams: IUser): Promise<IUser> {
    const user = new User(userParams);
    return user.save();
  }

  public findOneUser(query: any): Promise<IUser> {
    return User.findOne(query).exec();
  }

  public findOneUserAndPopulate(query: any): Promise<IUser> {
    return User.findOne(query).populate("Roles", "-__v").exec();
  }

  public updateUser(userParams: any): Promise<IUser> {
    const query = { _id: userParams._id };
    return User.findOneAndUpdate(query, userParams).exec();
  }  

  public findUserList(query: any): Promise<IUser[]> {
    return User.find(query).populate("Roles", "-__v").exec();
  }

  public deleteUser(_id: String): Promise<IUser> {
    const query = { _id: _id };
    return User.findByIdAndDelete(query).exec();
  }  
}
