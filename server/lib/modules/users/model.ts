import { IRole } from "../roles/model";
import * as mongoose from "mongoose";
import { ModificationNote } from "../common/model";

export interface IUser extends mongoose.Document {
  _id?: string;
  Username: string;
  Email: string;
  Password: string;
  Roles: IRole[];
  ModificationNotes: ModificationNote[];
}
