import * as mongoose from "mongoose";
import { ModificationNoteSchema } from "../common/model";
import { IUser } from "./model";

const Schema = mongoose.Schema;
const schema = new Schema(
  {
    Username: { type: String, required: true },
    Email: { type: String, required: true },
    Password: { type: String, required: true },
    Roles: [
      {
        type: Schema.Types.ObjectId,
        ref: "roles",
        required: true
      },
    ],
    ModificationNotes: [ModificationNoteSchema],
  },
  {
    collection: "users",
  }
);
const User = mongoose.model<IUser>("users", schema);
export default User;
