import * as mongoose from "mongoose";

export interface IRole extends mongoose.Document {
  _id?: string;
  Name: string;
}
