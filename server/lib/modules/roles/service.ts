import { IRole } from "./model";
import Role from "./schema";

export default class RoleService {
  public findRoleList(query: any): Promise<IRole[]> {
    return Role.find(query).exec();
  }

  public findOneRole(query: any): Promise<IRole> {
    return Role.findOne(query).exec();
  }
}
