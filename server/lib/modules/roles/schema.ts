import * as mongoose from "mongoose";
import { IRole } from "./model";

const Schema = mongoose.Schema;
const schema = new Schema(
  {
    Name: String
  },
  {
    collection: "roles",
  }
);
const Role = mongoose.model<IRole>("roles", schema);
export default Role;
