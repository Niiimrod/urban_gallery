import { ModificationNote } from "modules/common/model";
import { IGallery } from "modules/galleries/model";
import { IImage } from "modules/images/model";
import * as mongoose from "mongoose";

export interface IAlbum extends mongoose.Document {
  _id?: string;
  AlbumName: string;
  AlbumDescription: string;
  AlbumPhoto: IImage;
  AlbumGalleries: IGallery[];
  AlbumIsPublished: boolean;
  ModificationNotes: ModificationNote[];
}
