import * as mongoose from "mongoose";
import { ModificationNoteSchema } from "../common/model";
import { IAlbum } from "./model";

const Schema = mongoose.Schema;
const schema = new Schema(
  {
    AlbumName: String,
    AlbumDescription: String,
    AlbumPhoto: {
      type: Schema.Types.ObjectId,
      ref: "images",
    },
    AlbumGalleries: [
      {
        type: Schema.Types.ObjectId,
        ref: "galleries",
      },
    ],    
    AlbumIsPublished: Boolean,
    ModificationNotes: [ModificationNoteSchema],
  },
  {
    collection: "albums",
  }
);
const Album = mongoose.model<IAlbum>("albums", schema);
export default Album;
