import { IAlbum } from "./model";
import Album from "./schema";

export default class AlbumService {
  
  public getAlbumList(query: any): Promise<IAlbum[]> {
    return Album.find(query)
      .populate([
        {
          path: "AlbumPhoto",
        },
        {
          path: "AlbumGalleries",
          populate: {
            path: "GalleryPhoto GalleryImageList",
          },
        },
      ])
      .exec();
  }

  public createAlbum(albumParams: IAlbum): Promise<IAlbum> {
    const album = new Album(albumParams);
    return album.save();
  }

  public filterAlbum(query: any): Promise<IAlbum> {
    return Album.findOne(query)
      .populate([
        {
          path: "AlbumPhoto",
        },
        {
          path: "AlbumGalleries",
          populate: {
            path: "GalleryPhoto GalleryImageList",
          },
        },
      ])
      .exec();
  }

  public updateAlbum(albumParams: any, callback: any): void {
    const query = { _id: albumParams._id };
    Album.findOneAndUpdate(query, albumParams, callback);
  }

  public deleteAlbum(_id: String): Promise<void> {
    const query = { _id: _id };
    // Album.findByIdAndDelete(query, callback);
    return Album.remove(query).exec();
  }

  public addGalleryToAlbum(
    albumId: string,
    galleryParams: any
  ): Promise<IAlbum> {
    return Album.findByIdAndUpdate(albumId, {
      $push: { AlbumGalleries: galleryParams._id },
    }).exec();
  }
}
