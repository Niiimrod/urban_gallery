import { ModificationNote } from "modules/common/model";
import { IImage } from "modules/images/model";
import * as mongoose from "mongoose";

export interface IGallery extends mongoose.Document {
  _id?: string;
  GalleryName: string;
  GalleryDescription: string;
  GalleryPhoto: IImage;
  GalleryImageList: IImage[];
  ModificationNotes: ModificationNote[];
}
