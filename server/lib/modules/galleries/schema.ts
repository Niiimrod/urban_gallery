import * as mongoose from "mongoose";
import { ModificationNoteSchema } from "../common/model";
import { IGallery } from "./model";

const Schema = mongoose.Schema;
const schema = new Schema(
  {
    GalleryName: String,
    GalleryDescription: String,
    GalleryPhoto: {
      type: Schema.Types.ObjectId,
      ref: "images",
    },
    GalleryImageList: [
      {
        type: Schema.Types.ObjectId,
        ref: "images",
      },
    ],
    ModificationNotes: [ModificationNoteSchema],
  },
  {
    collection: "galleries",
  }
);
const Gallery = mongoose.model<IGallery>("galleries", schema);
export default Gallery;
