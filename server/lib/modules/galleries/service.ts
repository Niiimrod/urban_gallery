import { IGallery } from "./model";
import Gallery from "./schema";

export default class GalleryService {
  public getGalleryList(query: any): Promise<IGallery[]> {
    return Gallery.find(query)
      .populate([
        {
          path: "GalleryPhoto",
        },
        {
          path: "GalleryImageList",
        },
      ])
      .exec();
  }

  public createGallery(galleryParams: IGallery): Promise<IGallery> {
    const gallery = new Gallery(galleryParams);
    return gallery.save();
  }

  public filterGallery(query: any): Promise<IGallery> {
    return Gallery.findOne(query)
      .populate([
        {
          path: "GalleryPhoto",
        },
        {
          path: "GalleryImageList",
        },
      ])
      .exec();
  }

  public updateGallery(galleryParams: any): Promise<IGallery> {
    console.log("galleryParams", galleryParams);
    const query = { _id: galleryParams._id };
    return Gallery.findOneAndUpdate(query, galleryParams).exec();
  }

  public deleteGallery(_id: String, callback: any): void {
    const query = { _id: _id };
    Gallery.findByIdAndDelete(query, callback);
  }
}
