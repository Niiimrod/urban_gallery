import * as mongoose from "mongoose";

export interface ModificationNote {
  ModificationDate: Date;
  ModificationNote: String;
}

export const ModificationNoteSchema = new mongoose.Schema({
  ModificationDate: Date,
  ModificationNote: String,
});

export enum response_status_codes {
  success = 200,
  bad_request = 400,
  internal_server_error = 500,
  token_error = 403,
  unauthorized = 401,
  not_found = 404
}

export const RoleList = ["user", "admin", "moderator", "artist"];

export enum Roles {
  user = "user",
  admin = "admin",
  moderator = "moderator",
  artist = "artist"
}