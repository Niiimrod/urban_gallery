import { Response } from "express";
import { ModificationNote, response_status_codes } from "./model";

export function successResponse(
  message: string,
  DATA: any,
  res: Response
): void {
  res.status(response_status_codes.success).json({
    STATUS: "SUCCESS",
    MESSAGE: message,
    DATA,
  });
}

export function failureResponse(
  message: string,
  DATA: any,
  res: Response
): void {
  res.status(response_status_codes.bad_request).json({
    STATUS: "FAILURE",
    MESSAGE: message,
    DATA,
  });
}

export function insufficientParameters(res: Response): void {
  res.status(response_status_codes.bad_request).json({
    STATUS: "FAILURE",
    MESSAGE: "Insufficient parameters",
    DATA: {},
  });
}

export function mongoError(err: any, res: Response): void {
  res.status(response_status_codes.bad_request).json({
    STATUS: "FAILURE",
    MESSAGE: "MongoDB error",
    DATA: err,
  });
}

export function userAlreadyExist(res: Response): void {
  res.status(response_status_codes.bad_request).json({
    STATUS: "FAILURE",
    MESSAGE: "User already exist",
    DATA: {},
  });
}

export function roleDoesNotExist(message: string, res: Response): void {
  res.status(response_status_codes.bad_request).json({
    STATUS: "FAILURE",
    MESSAGE: message,
    DATA: {},
  });
}

export function tokenFailureResponse(res: Response): void {
  res.status(response_status_codes.token_error).json({
    STATUS: "FAILURE",
    MESSAGE: "No token provided",
    DATA: {},
  });
}

export function unauthorizedResponse(res: Response): void {
  res.status(response_status_codes.unauthorized).json({
    STATUS: "UNAUTHORIZED",
    MESSAGE: "Unauthorized!",
    DATA: {},
  });
}

export function unauthorizedTokenResponse(res: Response): void {
  res.status(response_status_codes.unauthorized).json({
    STATUS: "UNAUTHORIZED",
    MESSAGE: "Unauthorized!",
    DATA: {
      accessToken: null,
      message: "Invalid Password!",
    },
  });
}

export function roleRequiredResponse(message: string, res: Response): void {
  res.status(response_status_codes.token_error).json({
    STATUS: "ROLE_REQUIRED",
    MESSAGE: message,
    DATA: {},
  });
}

export function notFoundResponse(message: string, res: Response): void {
  res.status(response_status_codes.not_found).json({
    STATUS: "NOT_FOUND",
    MESSAGE: message,
    DATA: {},
  });
}

export function getModificationNote(message: string): ModificationNote {
  return {
    ModificationDate: new Date(Date.now()),
    ModificationNote: message,
  };
}
