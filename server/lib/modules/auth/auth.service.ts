import * as jwt from "jsonwebtoken";
import * as Config from "../../config/auth.config.json";

export default class AuthService {
  public verifyToken(token: any, callback: any) {
    jwt.verify(token, Config.secret, callback);
  }
}
