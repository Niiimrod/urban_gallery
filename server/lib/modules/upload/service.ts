import * as multer from "multer";
import { v4 as uuidv4 } from 'uuid';

export default class UploadService {
  private DIR = "./uploads/images/";

  private storage = multer.diskStorage({
    destination: (_req, _file, cb) => {
      cb(null, this.DIR);
    },
    filename: (_req, file, cb) => {
      const fileName = `${uuidv4()}`;
      cb(null, fileName);
    },
  });

  public upload = multer({
    storage: this.storage,
    fileFilter: (_req, file, cb) => {
      if (
        file.mimetype == "image/png" ||
        file.mimetype == "image/jpg" ||
        file.mimetype == "image/jpeg"
      ) {
        cb(null, true);
      } else {
        cb(null, false);
        return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
      }
    },
  });

  public uploadFile = (req): string[] | string => {
    const url = req.protocol + "://" + req.get("host");
    if (req.files && req.files.length) {
      const reqFiles = [];
      for (var i = 0; i < req.files.length; i++) {
        reqFiles.push(
          `${url}/uploads/images/${req.files[i].filename}`
        );
      }
      return reqFiles;
    } else {
      return `${url}/uploads/images/${req.file.filename}`;
    }
  };

  public getFileNameAndFileType = (req): { Name: string; Type: string } => {
    return {
      Name: req.file.filename,
      Type: req.file.mimetype,
    };
  };
}
