import { GalleryController } from "../controllers/gallery.controller";
import { Application } from "express";
import { AlbumController } from "../controllers/album.controller";
import { AuthController } from "../controllers/auth.controller";
import { TokenController } from "../controllers/token.controller";

export class AlbumRoutes {
  private albumController: AlbumController = new AlbumController();
  private tokenController: TokenController = new TokenController();
  private authController: AuthController = new AuthController();
  private galleryController: GalleryController = new GalleryController();
  
  public route(app: Application) {
    app.use((_, res, next) => {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });


    app.delete(
      "/api/admin/album/:albumId",
      [
        this.tokenController.verifyToken,
        this.authController.isAdminOrModerator,
      ],
      this.albumController.deleteAlbum
    );    

    app.get(
      "/api/albums",
      this.albumController.getAlbumList
    );

    app.get(
      "/api/albums/:albumId",
      this.albumController.getOneAlbum
    );

    app.post(
      "/api/admin/albums",
      [
        this.tokenController.verifyToken,
        this.authController.isAdminOrModerator,
      ],
      this.albumController.postOneAlbum
    );

  }
}
