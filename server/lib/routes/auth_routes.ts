import { Application } from "express";
import { AuthController } from "../controllers/auth.controller";
import { TokenController } from "../controllers/token.controller";

export class AuthRoutes {
  private authController: AuthController = new AuthController();
  private tokenController: TokenController = new TokenController();

  public route(app: Application) {
    app.use((_req, res, next) => {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    app.get(
      "/api/admin/istokenvalid",
      [this.tokenController.verifyToken, this.authController.isAdmin],
      this.authController.isTokenValid
    );

    app.post(
      "/api/auth/signup",
      [
        this.authController.checkDuplicateUsernameOrEmail,
        this.authController.checkRolesExisted,
      ],
      this.authController.signUp
    );

    app.post("/api/auth/signin", this.authController.signIn);
  }
}
