import { Application } from "express";
import { AuthController } from "../controllers/auth.controller";
import { GalleryController } from "../controllers/gallery.controller";
import { TokenController } from "../controllers/token.controller";

export class GalleryRoutes {
  private tokenController: TokenController = new TokenController();
  private authController: AuthController = new AuthController();
  private galleryController: GalleryController = new GalleryController();

  public route(app: Application) {
    app.use((_, res, next) => {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    app.post(
      "/api/admin/albums/:albumId/gallery",
      [this.tokenController.verifyToken, this.authController.isAdmin],
      this.galleryController.postOneAlbumGallery
    );

    app.put(
      "/api/admin/albums/:albumId/gallery/:galleryId",
      [this.tokenController.verifyToken, this.authController.isAdmin],
      this.galleryController.putGallery
    );    

    app.get(
      "/api/album/:albumId/galleries",
      this.galleryController.getAlbumGalleryList
    );  
    
    app.get(
      "/api/album/:albumId/gallery/:galleryId",
      this.galleryController.getOneGallery
    );        
  }
}
