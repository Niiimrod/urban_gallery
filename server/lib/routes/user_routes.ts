import { Application } from "express";
import { AuthController } from "../controllers/auth.controller";
import { TokenController } from "../controllers/token.controller";
import { UserController } from "../controllers/user.controller";

export class UserRoutes {
  private tokenController: TokenController = new TokenController();
  private authController: AuthController = new AuthController();
  private userController: UserController = new UserController();

  public route(app: Application) {
    app.use((_, res, next) => {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    app.get(
      "/api/admin/users",
      [
        this.tokenController.verifyToken,
        this.authController.isAdminOrModerator,
      ],
      this.userController.getUserList
    );

    app.post(
      "/api/admin/users",
      [
        this.tokenController.verifyToken,
        this.authController.isAdminOrModerator,
        this.authController.checkDuplicateUsernameOrEmail,
        this.authController.checkRolesExisted,
      ],
      this.userController.postUser
    );

    app.put(
      "/api/admin/users/:id",
      [this.tokenController.verifyToken],
      this.userController.putUser
    );

    app.delete(
      "/api/admin/users/:id",
      [
        this.tokenController.verifyToken,
        this.authController.isAdminOrModerator,
      ],
      this.userController.deleteUser
    );

    app.get(
      "/api/admin/users/:id",
      [this.tokenController.verifyToken],
      this.userController.getOneUser
    );
  }
}
