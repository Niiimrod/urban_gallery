import { Application } from "express";
import { UploadController } from "../controllers/upload.controller";
import UploadService from "../modules/upload/service";

export class UploadRoutes {
  private uploadService: UploadService = new UploadService();
  private uploadController: UploadController = new UploadController();

  public route(app: Application) {
    app.use((_, res, next) => {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    app.post(
      "/api/admin/upload/single",
      this.uploadService.upload.single("file"),
      this.uploadController.postImage
    );

    app.post(
        "/api/admin/upload/multiple",
        this.uploadService.upload.array("files", 10),
        this.uploadController.postImageList
      );
  }
}
