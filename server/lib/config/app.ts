import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as mongoose from "mongoose";
import environment from "../environment";
import Role from "../modules/roles/schema";
import { AlbumRoutes } from "../routes/album_routes";
import { AuthRoutes } from "../routes/auth_routes";
import { CommonRoutes } from "../routes/common_routes";
import { GalleryRoutes } from "../routes/gallery_routes";
import { UploadRoutes } from "../routes/upload_routes";
import { UserRoutes } from "../routes/user_routes";
class App {
  public app: express.Application;
  public mongoUrl: string = `mongodb://127.0.0.1:27017/${environment.getDBName()}`;
  private user_routes: UserRoutes = new UserRoutes();
  private auth_routes: AuthRoutes = new AuthRoutes();
  private upload_routes: UploadRoutes = new UploadRoutes();
  private album_routes: AlbumRoutes = new AlbumRoutes();
  private gallery_routes: GalleryRoutes = new GalleryRoutes();
  private common_routes: CommonRoutes = new CommonRoutes();

  constructor() {
    this.app = express();
    this.loadConfig();
    this.loadMongoSetup();
    this.user_routes.route(this.app);
    this.auth_routes.route(this.app);
    this.upload_routes.route(this.app);
    this.album_routes.route(this.app);
    this.gallery_routes.route(this.app);
    //keep it last routes
    this.common_routes.route(this.app);
  }

  private loadConfig(): void {
    this.app.use("/uploads", express.static(process.cwd() + "/uploads"));
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
  }

  private loadMongoSetup(): void {
    mongoose.connect(
      this.mongoUrl,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      },
      () => {
        this.initial();
      }
    );
  }

  private initial() {
    Role.estimatedDocumentCount({}, (err: any, count: any) => {
      if (!err && count === 0) {
        new Role({
          Name: "user",
        }).save((err) => {
          if (err) {
            console.log("error", err);
          }

          console.log("added 'user' to roles collection");
        });

        new Role({
          Name: "moderator",
        }).save((err) => {
          if (err) {
            console.log("error", err);
          }

          console.log("added 'moderator' to roles collection");
        });

        new Role({
          Name: "admin",
        }).save((err) => {
          if (err) {
            console.log("error", err);
          }
          console.log("added 'admin' to roles collection");
        });
      }
    });
  }
}
export default new App().app;
